﻿# System nadzoru kierowcy

Aplikacja mobilna zdolna do oceny zachowań kierowcy w oraz alarmowania go w razie niebezpieczeństwa.   
  
Program jest w stanie wykryć zachowania kierowcy, takie jak:  
•zaśnięcie,  
•odwrócenie, pochylenie głowy,  
•trzymanie dłoni przy twarzy:  
    –rozmawianie przez telefon,  
    –jedzenie i picie,  
    –używanie grzebienia,
•zniknięcie z pola widzenia kamery.  
  
Aplikacja została zaimplementowana na system *Android* w języku *Java*.   
Do realizacji zagadnień związanych z komunikacją z kamerą użyto biblioteki *camera2*.   
Narzędzie, które zapewniło detekcję twarzy oraz pozy to *Google ML Kit*.

Film przedstawiający działanie aplikacji:
[LINK](https://drive.google.com/file/d/1NrHJ4t03K5qFEzho7kpIk_6pRkg5rE3a/view?usp=sharing)

Filmy użyte w fazie testowania:
[LINK](https://drive.google.com/drive/folders/1JtKNwb_Wki1eyunH1M3U2zFTrC2Olsee?usp=sharing)

