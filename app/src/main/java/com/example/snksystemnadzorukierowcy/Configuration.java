package com.example.snksystemnadzorukierowcy;

/**
 * Klasa zawierająca parametry konfiguracyjne programu.
 */
public abstract class Configuration {

    /**
     * Maksymalna częstotliwość, z jaką kamera przetwarza obraz.
     */
    public static final int MAX_FPS_LIMIT = 25;

    /**
     * Minimalne poziom pewności wykrycia punktów pozy.
     * Np. jeśli wynosi 0.75f, to rysowane będą tylko te punkty,
     * które są w 75% poprawnie wykryte przez detektor.
     */
    public static final float POSE_LANDMARK_MINIMUM_BORDER_IN_FRAME_LIKELIHOOD = 0.60f;

    /**
     * Określa ile sekund program ma czekać zanim rozpocznie kalibrację.
     */
    public static final int CALIBRATION_PREPARATION_TIME = 5;

    /**
     * Określa ile sekund ma trwać kalibracja programu.
     */
    public static final int CALIBRATION_TIME = 5;

    /**
     * Określa co ile klatek detektor pozy powinien być załączany.
     */
    public static final int POSE_DETECTION_FREQUENCY = 2;

    /**
     * Maksymalne pochylenie głowy. Kąt Eulera X.
     * Większe kąty będą zaliczane jako niebezpieczne.
     */
    public static final int MAXIMUM_HEAD_TILT_X = 14;

    /**
     * Maksymalne pochylenie głowy. Kąt Eulera Y.
     * Większe kąty będą zaliczane jako niebezpieczne.
     */
    public static final int MAXIMUM_HEAD_TILT_Y = 23;

    /**
     * Maksymalne pochylenie głowy. Kąt Eulera Z.
     * Większe kąty będą zaliczane jako niebezpieczne.
     */
    public static final int MAXIMUM_HEAD_TILT_Z = 15;

    /**
     * Minimalny współczynnik pewności kwalifikujący oczy jako otwarte.
     */
    public static final float MINIMUM_EYES_OPEN_PROBABILITY = 0.52f;

    /**
     * Minimalny współczynnik pewności kwalifikujący twarz jako uśmiechniętą.
     */
    public static final float MINIMUM_SMILING_PROBABILITY = 0.75f;


    /**
     * Określa po ilu sekundach pod wykryciu przechylenia głowy ma się włączyć alarm.
     */
    public static final int WARNING_DURATION_HEAD_TILT = 1;

    /**
     * Określa po ilu sekundach od wykrycia zamknięcia oczu ma się włączyć alarm.
     */
    public static final int WARNING_DURATION_EYES_CLOSED = 1;

    /**
     * Określa po ilu sekundach od wykrycia prawidłowego pochylenia głowy, poziom ostrzeżeń ma się wyzerować.
     */
    public static final int GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME = 2;

    /**
     * Określa po ilu <b>milisekundach</b> od wykrycia otwarcia oczy, poziom ostrzeżeń ma się wyzerować.
     */
    public static final float GUARD_EYES_CLOSED_WARNING_TURN_OFF_TIME = 0.75f;

    /**
     * Określa po ilu sekundach od zniknięcia twarzy z pola widzenia ma się włączyć alarm.
     */
    public static final int WARNING_DURATION_FACE_NOT_PRESENT = 1;

    /**
     * Określa po ilu sekundach od wykrycia twarzy, poziom ostrzeżeń ma się wyzerować.
     */
    public static final int GUARD_FACE_NOT_PRESENT_WARNING_TURN_OFF_TIME = 1;

    /*
     * Maksymalna odległość dłoni od nosa, która jest uznawana za niebezpieczną.
     * Jest to wielokrotność dystansu między lewym, a prawym uchem.
     * Jeśli odległość dłoń-nos będzie mniejsza, podniesie się poziom ostrzeżeń.
     */
    public static final float POSE_CLASSIFICATION_HAND_NEAR_HEAD_DETECTION_THRESHOLD = 2.38f;

    /**
     * Kąt zgięcia ręki w łokciu, poniżej którego ręka jest uznawana za zgiętą.
     */
    public static final int POSE_CLASSIFICATION_BENT_ARM_ANGLE_THRESHOLD = 59;

    /**
     * Określa po ilu sekundach od wykrycia dłoni przy twarzy ma się włączyć alarm.
     */
    public static final int WARNING_DURATION_HAND_NEAR_HEAD = 1;

    /**
     * Określa po ilu sekundach od wykrycia poprawnie ułożonej ręki, poziom ostrzeżeń ma się wyzerować.
     */
    public static final int GUARD_HAND_NEAR_HEAD_TURN_OFF_TIME = 1;

    /**
     * Określa co ile klatek nagranie powinno być przetwarzane.
     * Np. jeśli =1, przetwarzana jest każda klatka po kolei.
     */
    public static final int RECORD_ANALYZING_FRAME_STEP = 2;
}