package com.example.snksystemnadzorukierowcy.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCaptureSession.CaptureCallback;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.OutputConfiguration;
import android.hardware.camera2.params.SessionConfiguration;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.snksystemnadzorukierowcy.R;
import com.example.snksystemnadzorukierowcy.utils.file.FileFormat;
import com.example.snksystemnadzorukierowcy.utils.file.FileService;
import com.example.snksystemnadzorukierowcy.utils.image.ImageSaver;
import com.example.snksystemnadzorukierowcy.utils.OptimalSizeChooser;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

//https://www.youtube.com/watch?v=-Ry_V9VthTk&list=PL9jCwTXYWjDIHNEGtsRdCTk79I9-95TbJ&index=5
public class CameraPreviewActivity extends AppCompatActivity {

    private static final int CAMERA_PERMISSION_REQUEST_CODE = 123;
    private static final int WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 124;
    private static final int STATE_PREVIEW = 0;
    private static final int STATE_WAIT_LOCK = 1;
    private int mCaptureState = STATE_PREVIEW;

    private final int[] previewSize = new int[2]; // rozmiar pola, na którym jest wyświetlany podgląd z kamery POBIERAMY TO OD RAZU Z XMLa

    private TextureView mTextureView;
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(@NonNull SurfaceTexture surface, int width, int height) {
            previewSize[0] = width;
            previewSize[1] = height;
            setUpCamera(width, height);
            connectCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(@NonNull SurfaceTexture surface, int width, int height) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(@NonNull SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(@NonNull SurfaceTexture surface) {
        }
    };
    private CameraDevice mCameraDevice;
    private final CameraDevice.StateCallback mCameraDeviceStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) { // wchodzimy tu gdy odpalimy juz kamere ( cameraManager.openCamera() w metodzie connectCamera() )
            mCameraDevice = camera;
            if (mIsRecording) {
                mVideoFileName = fileService.createFileName(FileFormat.MP4);
                startRecord(); // https://youtu.be/69J2ycNCtpE?t=489 <- wyjaśnienie po co robimy te operacje przy nagrywaniu
                mMediaRecorder.start();
            } else {
                startPreview();
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            camera.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            camera.close();
            mCameraDevice = null;
        }
    };
    // odpalam dodatkowy wątek, aby nie odciążyć główny wątek (aby UI się nie zacięło)
    private HandlerThread mBackgroundHandlerThread;
    private Handler mBackgroundHandler;
    private String mCameraId;
    private int mCameraFacing = CameraCharacteristics.LENS_FACING_FRONT; // kamera, której aktualnie używamy
    private Size mPreviewSize; // rozmiar podglądu kamery
    private Size mVideoSize; // rozdzielczość nagrania
    private Size mImageSize; // rozdzielczosc obrazu/zdjęcia przechwytywanego z kamery

    private ImageReader mImageReader; // tu będą trafiać przechwycone obrazy, które można przetwarzać
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() { // listener będzie odpalany wtedy, gdy obraz będzie przechwycony
        @Override
        public void onImageAvailable(ImageReader reader) {
            mBackgroundHandler.post(new ImageSaver(reader.acquireLatestImage(), mImageFileName));
        }
    };

    private MediaRecorder mMediaRecorder;

    private int mTotalRotation;
    private CameraCaptureSession mPreviewCaptureSession;
    private final CameraCaptureSession.CaptureCallback mCaptureCallback = new CaptureCallback() { // używamy tego callbacka do przechwytywania obrazów kiedy jesteśmy w preview-mode

        private void process(CaptureResult captureResult) {
            switch (mCaptureState) {
                case STATE_PREVIEW:
                    //do nothing
                    break;
                case STATE_WAIT_LOCK:
                    mCaptureState = STATE_PREVIEW;
                    startStillCaptureRequest();
                    break;
            }
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
            process(result);
        }
    };

    private CaptureRequest.Builder mCaptureRequestBuilder; // tutaj przechowamy wszystkie informacje zanim zrobimy requesta do kamery
    private ImageButton mRecordImageButton;
    private ImageButton mStillImageButton;
    private boolean mIsRecording = false;

    private final FileService fileService = new FileService();
    private String mVideoFileName;
    private String mImageFileName;

    private static final SparseIntArray ORIENTATIONS = new SparseIntArray(); // pomaga przeliczyć orientację urządzenia (która jest enumem) na kąty

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_preview);

        fileService.createCustomFolder();

        mMediaRecorder = new MediaRecorder();

        mTextureView = findViewById(R.id.textureView);

        mStillImageButton = findViewById(R.id.cameraImageButton);
        mStillImageButton.setOnClickListener(v -> lockFocus());

        mRecordImageButton = findViewById(R.id.videoOnlineButton);
        mRecordImageButton.setOnClickListener(v -> {
            if (mIsRecording) {
                mIsRecording = false;
                mRecordImageButton.setImageResource(android.R.drawable.presence_video_online);
                try {
                    mMediaRecorder.stop();
                    mMediaRecorder.reset();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } finally {
                    mMediaRecorder.release();
                    mMediaRecorder = null;
//                    mMediaRecorder.reset(); // zatrzymujemy nagrywanie
                }
                startPreview(); // włączamy z powrotem podgląd NA ŻYWO
            } else {
                mMediaRecorder = new MediaRecorder();
                checkWriteStoragePermission();
            }
        });

        findViewById(R.id.camera_switch_btn).setOnClickListener(view -> { // zmiana aktywnej kamery
            runOnUiThread(() -> {
                String name = view.getTag().toString();
                final int id = getResources().getIdentifier(name, "anim", getPackageName());
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), id);
                view.startAnimation(animation);
            });
            toggleCamera();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        startBackgroundThread();

        if (mTextureView.isAvailable()) {
            setUpCamera(mTextureView.getWidth(), mTextureView.getHeight());
            connectCamera();
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    protected void onPause() {
        closeCamera();
        stopBackgroundThread();

        super.onPause();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) { // robimy tu fullscreen
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE // FIXME
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            );
        }
    }

    private void setUpCamera(final int width, final int height) {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId); // reprezentuje wlasciwosci poszczegolnej kamery
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == mCameraFacing) {
                    continue;
                }
                StreamConfigurationMap availableResolutionsMap = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP); // lista rozdzielczosci dostepnych z kamery
                final int deviceOrientation = getApplicationContext().getDisplay().getRotation();
                mTotalRotation = ORIENTATIONS.get(deviceOrientation);
                int rotatedWidth;
                int rotatedHeight;
                if (mTotalRotation == 90 || mTotalRotation == 270) { // czy należy obrócić obraz (zamienić wysokość z szerokością)
                    rotatedWidth = height;
                    rotatedHeight = width;
                } else {
                    rotatedWidth = width;
                    rotatedHeight = height;
                }
                mPreviewSize = OptimalSizeChooser.chooseOptimalSize(availableResolutionsMap.getOutputSizes(SurfaceTexture.class), rotatedWidth, rotatedHeight);
                mVideoSize = OptimalSizeChooser.chooseOptimalSize(availableResolutionsMap.getOutputSizes(MediaRecorder.class), rotatedWidth, rotatedHeight);
                mImageSize = OptimalSizeChooser.chooseOptimalSize(availableResolutionsMap.getOutputSizes(ImageFormat.JPEG), rotatedWidth, rotatedHeight);

                mImageReader = ImageReader.newInstance(mImageSize.getWidth(), mImageSize.getHeight(), ImageFormat.JPEG, 1);
                mImageReader.setOnImageAvailableListener(mOnImageAvailableListener, mBackgroundHandler);
                mCameraId = cameraId;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void connectCamera() {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
            } else {
                cameraManager.openCamera(mCameraId, mCameraDeviceStateCallback, mBackgroundHandler); // dzieki zastosowaniu wlasnego handlera, operacje z kamerą dzieją się na oddzielnym wątku
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void closeCamera() {
        if (mCameraDevice != null) {
            mCameraDevice.close();
            mCameraDevice = null;
        }
    }

    private void toggleCamera() { // przełącza kamery między przednią a tylnią
        if (mCameraFacing == CameraCharacteristics.LENS_FACING_FRONT) {
            mCameraFacing = CameraCharacteristics.LENS_FACING_BACK;
        } else if (mCameraFacing == CameraCharacteristics.LENS_FACING_BACK) {
            mCameraFacing = CameraCharacteristics.LENS_FACING_FRONT;
        }
        closeCamera();
        setUpCamera(previewSize[0], previewSize[1]);
        connectCamera();
//        startPreview();
    }

    private void startRecord() {
        try {
            setUpMediaRecorder();
            SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture(); // pobieram surfaceTexture z textureView i na klasie Surface bede umieszczal podglad z kamery
            surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            Surface previewSurface = new Surface(surfaceTexture);
            Surface recordSurface = mMediaRecorder.getSurface();
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            mCaptureRequestBuilder.addTarget(previewSurface);
            mCaptureRequestBuilder.addTarget(recordSurface);

            OutputConfiguration previewOutputConfiguration = new OutputConfiguration(previewSurface);
            OutputConfiguration recordOutputConfiguration = new OutputConfiguration(recordSurface);
            OutputConfiguration imageReaderOutputConfiguration = new OutputConfiguration(mImageReader.getSurface());
            SessionConfiguration sessionConfiguration = new SessionConfiguration(
                    SessionConfiguration.SESSION_REGULAR,
                    List.of(previewOutputConfiguration, recordOutputConfiguration, imageReaderOutputConfiguration),
                    Runnable::run,
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mPreviewCaptureSession = session;
                            try {
                                mPreviewCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), null, null);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            Toast.makeText(getApplicationContext(), "Wystąpił błąd podczas nawiązywania sesji kamery.", Toast.LENGTH_SHORT).show();
                        }
                    }
            );
            mCameraDevice.createCaptureSession(sessionConfiguration);
        } catch (IOException | CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void startPreview() { // podgląd z kamery
        SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture(); // pobieram surfaceTexture z textureView i na klasie Surface bede umieszczal podglad z kamery
        surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
        Surface previewSurface = new Surface(surfaceTexture);
        try {
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mCaptureRequestBuilder.addTarget(previewSurface);
            //tutaj startujemy sesję
            OutputConfiguration outputConfiguration = new OutputConfiguration(previewSurface);
            OutputConfiguration imageReaderOutputConfiguration = new OutputConfiguration(mImageReader.getSurface());
            SessionConfiguration sessionConfiguration = new SessionConfiguration(
                    SessionConfiguration.SESSION_REGULAR, // FIXME zobaczyc czemu nie dziala 'SessionConfiguration.FAST_MODE'
                    Arrays.asList(outputConfiguration, imageReaderOutputConfiguration),
                    Runnable::run,
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mPreviewCaptureSession = session;
                            try {
                                mPreviewCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), mCaptureCallback, mBackgroundHandler); // drugi argument posłuży nam z przetwarzaniem obrazu
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            Toast.makeText(getApplicationContext(), "Konfiguracja sesji kamery sie nie udala", Toast.LENGTH_SHORT).show();
                        }
                    }
            );
            mCameraDevice.createCaptureSession(sessionConfiguration);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void startStillCaptureRequest() {
        try {
            if (mIsRecording) {
                mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_VIDEO_SNAPSHOT);
            } else {
                mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            }
            mCaptureRequestBuilder.addTarget(mImageReader.getSurface());
            if (mCameraFacing == CameraCharacteristics.LENS_FACING_FRONT) {
                mCaptureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, mTotalRotation + 90);
            } else {
                mCaptureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, mTotalRotation + 270);
            }

            CameraCaptureSession.CaptureCallback captureCallback = new CaptureCallback() {
                @Override
                public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
                    super.onCaptureStarted(session, request, timestamp, frameNumber);
                    mImageFileName = fileService.createFileName(FileFormat.JPEG);
                }
            };
            mPreviewCaptureSession.capture(mCaptureRequestBuilder.build(), captureCallback, null); // juz wczesniej jestesmy w odzielnym wątku, więc dajemy null jako handler,
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        "Aplikacja nie będzie działać poprawnie bez zgody na robienie zdjęć i nagrywanie filmów", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mIsRecording = true;
                mRecordImageButton.setImageResource(android.R.drawable.presence_video_busy);
                mVideoFileName = fileService.createFileName(FileFormat.MP4);
            }
        }
    }

    private void startBackgroundThread() {
        mBackgroundHandlerThread = new HandlerThread("mojWatek");
        mBackgroundHandlerThread.start();
        mBackgroundHandler = new Handler(mBackgroundHandlerThread.getLooper());
    }

    private void stopBackgroundThread() {
        mBackgroundHandlerThread.quitSafely();
        try {
            mBackgroundHandlerThread.join();
            mBackgroundHandlerThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void checkWriteStoragePermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            mIsRecording = true;
            mRecordImageButton.setImageResource(android.R.drawable.presence_video_busy);
            mVideoFileName = fileService.createFileName(FileFormat.MP4);
            startRecord();
            mMediaRecorder.start();
        } else {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
        }
    }

    private void setUpMediaRecorder() throws IOException {
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setOutputFile(mVideoFileName);
        mMediaRecorder.setVideoEncodingBitRate(1000000);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setVideoSize(mVideoSize.getWidth(), mVideoSize.getHeight());
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        if (mCameraFacing == CameraCharacteristics.LENS_FACING_FRONT) {
            mMediaRecorder.setOrientationHint(mTotalRotation + 90);
        } else {
            mMediaRecorder.setOrientationHint(mTotalRotation + 270);
        }
        mMediaRecorder.prepare();
    }

    private void lockFocus() { // funkcja zatrzyma podgląd kamery na żywo
        mCaptureState = STATE_WAIT_LOCK;
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CaptureRequest.CONTROL_AF_TRIGGER_START);
        try {
            mPreviewCaptureSession.capture(mCaptureRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
}