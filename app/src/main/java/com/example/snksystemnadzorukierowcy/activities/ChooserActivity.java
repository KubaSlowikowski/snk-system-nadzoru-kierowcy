package com.example.snksystemnadzorukierowcy.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.snksystemnadzorukierowcy.R;

public class ChooserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooser);

        findViewById(R.id.snkLiveDetectionButton).setOnClickListener(view -> startActivity(new Intent(this, SNKLivePreviewActivity.class)));
//        findViewById(R.id.camera_preview_btn).setOnClickListener(view -> startActivity(new Intent(this, CameraPreviewActivity.class)));
        findViewById(R.id.record_analyzing_btn).setOnClickListener(view -> startActivity(new Intent(this, SNKRecordAnalyzingActivity.class)));
    }
}