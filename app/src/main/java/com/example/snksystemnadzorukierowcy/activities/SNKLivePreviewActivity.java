package com.example.snksystemnadzorukierowcy.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.OutputConfiguration;
import android.hardware.camera2.params.SessionConfiguration;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Range;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.snksystemnadzorukierowcy.Configuration;
import com.example.snksystemnadzorukierowcy.R;
import com.example.snksystemnadzorukierowcy.detectors.DetectorParam;
import com.example.snksystemnadzorukierowcy.detectors.MainDetector;
import com.example.snksystemnadzorukierowcy.detectors.SNKDetector;
import com.example.snksystemnadzorukierowcy.utils.OptimalSizeChooser;
import com.example.snksystemnadzorukierowcy.utils.SoundUtils;
import com.example.snksystemnadzorukierowcy.utils.WorkingMode;

import java.util.Arrays;

public class SNKLivePreviewActivity extends AppCompatActivity {

    private static final int CAMERA_PERMISSION_REQUEST_CODE = 123;

    /**
     * Widok, na którym będą wyświetlane informacje dotyczące parametrów pracy detektora
     */
    private TextView mDetectorDelayTextView;
    /**
     * Widok, na którym będą wyświetlane informacje dotyczące parametrów wykrytej twarzy,
     * np. kąty rotacji oraz prawdopodobieństwa zamkniętych oczu.
     */
    private TextView mFaceParametersTextView;

    /**
     * Widok na środku ekranu, na którym będzie wyświetlane odliczanie czasu.
     */
    private TextView mCountDownTextView;

    /**
     * Widok znajdujący się nad countDownTextView, zawierający dodatkowe informacje.
     */
    private TextView mCountDownInfoTextView;

    /**
     * Widok, na którym będą rysowane wykryte przez detektor obiekty
     */
    private ImageView mImageView;

    /**
     * Widok, na którym będzie obraz z kamery
     */
    private TextureView mTextureView;
    private final int[] mTextureViewSurfaceSize = new int[2];
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(@NonNull SurfaceTexture surface, int width, int height) {
            mTextureViewSurfaceSize[0] = width;
            mTextureViewSurfaceSize[1] = height;
            setUpCamera(width, height);
            connectCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(@NonNull SurfaceTexture surface, int width, int height) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(@NonNull SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(@NonNull SurfaceTexture surface) {
        }
    };

    /**
     * Oddzielny wątek działający w tle, który ma na celu odciążenie głównego wątka.
     * Pozwala na płynne działanie UI
     */
    private HandlerThread mBackgroundHandlerThread;
    private Handler mBackgroundHandler;

    /**
     * ID kamery.
     */
    private String mCameraId;

    private CameraDevice mCameraDevice;

    /**
     * Callback, który po nawiązaniu połączenia z kamerą podemuje kolejne działania
     * jak m.in. start podglądu na żywo.
     */
    private final CameraDevice.StateCallback mCameraDeviceStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            mCameraDevice = camera;
            startPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            camera.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            camera.close();
            mCameraDevice = null;
        }
    };

    /**
     * Kamera, której aktualnie używamy (przednia, tylna, itd.).
     */
    private int mCameraFacing = CameraCharacteristics.LENS_FACING_BACK;

    /**
     * Rozdzielczość podglądu obrazu z kamery, który będzie wyświetlany 'mTextureView'.
     */
    private Size mPreviewSize;

    /**
     * Rozdzielczość obrazu, który będzie przechwycony z kamery i dalej przetwarzany.
     */
    private Size mImageSize;

    private int mRotation;

    /**
     * Zmienna przechowujące wszystkie informacje, referencje potrzebne do poprawnej pracy detektora.
     */
    private DetectorParam detectorParam;

    /**
     * Główny detektor, który zawiera w sobie wszystkie potrzebne detektory i decyduje o ich działaniu.
     */
    private MainDetector detector;

    /**
     * Służy do odczytywania obrazów z sesji kamery.
     */
    private ImageReader mImageReader;
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(ImageReader reader) {

            if (detector == null || detector.getShouldWait()) {
                reader.acquireLatestImage().close();
                return;
            }

            Image image = reader.acquireLatestImage();
            if (image == null) {
                return;
            }

            detectorParam.update(image, mRotation);

            detector.process(detectorParam);
        }
    };

    /**
     * Pomocnicza tablica, która pomoga w przeliczaniu orientacji urządzenia (typ wyliczeniowy) na kąty.
     */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    public SNKLivePreviewActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snk_live_preview);

        mDetectorDelayTextView = findViewById(R.id.live_dtn_detector_delay_text_view);
        mFaceParametersTextView = findViewById(R.id.current_face_parameters_text_view);
        mCountDownTextView = findViewById(R.id.live_dtn_calibration_number_in_center_text_view);
        mCountDownInfoTextView = findViewById(R.id.live_dtn_calibration_text_above_center_text_view);

        mImageView = findViewById(R.id.live_dtn_image_view);

        mTextureView = findViewById(R.id.texture_view);

        findViewById(R.id.live_dtn_start_calibration_button).setOnClickListener(createCountDownButtonListener());
        findViewById(R.id.toggle_camera_btn).setOnClickListener(view -> {
            runOnUiThread(() -> {
                String name = view.getTag().toString();
                final int id = getResources().getIdentifier(name, "anim", getPackageName());
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), id);
                view.startAnimation(animation);
            });
            toggleCamera();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        startBackgroundThread();
        if (mTextureView.isAvailable()) {
            setUpCamera(mTextureView.getWidth(), mTextureView.getHeight());
            connectCamera();
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener); // tu wchodzimy przy pierwszym uruchomieniu aktywności
        }
    }

    @Override
    protected void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    private View.OnClickListener createCountDownButtonListener() {
        return buttonView -> {
            buttonView.setVisibility(View.INVISIBLE);

            mCountDownInfoTextView.setText(R.string.calibration_preparation_countdown_info);

            new CountDownTimer(Configuration.CALIBRATION_PREPARATION_TIME * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    SoundUtils.beep();
                    mCountDownTextView.setText(String.valueOf(millisUntilFinished / 1000));
                }

                @Override
                public void onFinish() {
                    SoundUtils.doubleBeep();
                    mCountDownTextView.setVisibility(View.INVISIBLE);
                    mCountDownInfoTextView.setVisibility(View.INVISIBLE);
                    detector = SNKDetector.getInstance();
                }
            }.start();
        };
    }

    private void startBackgroundThread() {
        mBackgroundHandlerThread = new HandlerThread("face_detection_activity_background_thread");
        mBackgroundHandlerThread.start();
        mBackgroundHandler = new Handler(mBackgroundHandlerThread.getLooper());
    }

    private void stopBackgroundThread() {
        mBackgroundHandlerThread.quitSafely();
        try {
            mBackgroundHandlerThread.join();
            mBackgroundHandlerThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Dokonuje wstępnych ustawień kamery np. dobranie odpowiedniej rozdzielczości obrazu pasującej do ekranu telefonu.
     *
     * @param width  Szerokość TextureView, do którego podpinamy podgląd z kamery
     * @param height Wysokość TextureView, do którego podpinamy podgląd z kamery
     */
    private void setUpCamera(int width, int height) {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == mCameraFacing) {
                    continue;
                }

                mRotation = getRotationCompensation(cameraManager, cameraId);

                StreamConfigurationMap availableResolutionsMap = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                mPreviewSize = OptimalSizeChooser.chooseOptimalSize(availableResolutionsMap.getOutputSizes(SurfaceTexture.class), width, height);
                mImageSize = OptimalSizeChooser.chooseOptimalSize(availableResolutionsMap.getOutputSizes(ImageFormat.JPEG), width, height);

                mImageReader = ImageReader.newInstance(mImageSize.getWidth(), mImageSize.getHeight(), ImageFormat.YUV_420_888, 2);
                mImageReader.setOnImageAvailableListener(mOnImageAvailableListener, mBackgroundHandler);

                mCameraId = cameraId;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Zwraca kąt, o jaki obraz musi zostać obrócony w zależności od aktualnej orientacji urządzenia
     *
     * @return Kąt, o jaki obraz musi zostać obrócony w zależności od aktualnej orientacji urządzenia
     */
    private int getRotationCompensation(CameraManager cameraManager, String cameraId) {
        int rotationCompensation = -1;
        try {
            // Get the device's current rotation relative to its "native" orientation.
            // Then, from the ORIENTATIONS table, look up the angle the image must be
            // rotated to compensate for the device's rotation.
            int deviceOrientation = getApplicationContext().getDisplay().getRotation();
            rotationCompensation = ORIENTATIONS.get(deviceOrientation);

            // Get the device's sensor orientation.
            int sensorOrientation = cameraManager
                    .getCameraCharacteristics(cameraId)
                    .get(CameraCharacteristics.SENSOR_ORIENTATION);

            if (mCameraFacing == CameraCharacteristics.LENS_FACING_BACK) {
                rotationCompensation = (sensorOrientation - rotationCompensation + 360) % 360;
            } else {
                rotationCompensation = (sensorOrientation + rotationCompensation) % 360;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return rotationCompensation;
    }

    /**
     * Prosi o zgodę na użycie kamery. Po jej otrzymaniu nawiązuje połączenie z kamerą.
     * Należy ją wywołać po metodzie setUpCamera().
     */
    private void connectCamera() {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
            } else {
                cameraManager.openCamera(mCameraId, mCameraDeviceStateCallback, null); // dzieki zastosowaniu wlasnego handlera, operacje z kamerą dzieją się na oddzielnym wątku
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Kończy działanie kamery.
     */
    private void closeCamera() {
        if (mCameraDevice != null) {
            mCameraDevice.close();
            mCameraDevice = null;
        }
    }

    /**
     * Przełącza między przednią a tylną kamerą
     */
    private void toggleCamera() {
        if (mCameraFacing == CameraCharacteristics.LENS_FACING_FRONT) {
            mCameraFacing = CameraCharacteristics.LENS_FACING_BACK;
        } else if (mCameraFacing == CameraCharacteristics.LENS_FACING_BACK) {
            mCameraFacing = CameraCharacteristics.LENS_FACING_FRONT;
        }
        closeCamera();
        mDetectorDelayTextView.setText("");
        mFaceParametersTextView.setText("");
        detectorParam.setCameraFacing(mCameraFacing);
        setUpCamera(mTextureViewSurfaceSize[0], mTextureViewSurfaceSize[1]);
        connectCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        R.string.live_dtn_no_permission_PHOTO_VIDEOS,
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Pobiera obraz z kamery i na żywo wyświetla go na ekranie.
     */
    private void startPreview() {
        SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture(); // pobieram surfaceTexture z textureView i na klasie Surface bede umieszczal podglad z kamery
        surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
        Surface previewSurface = new Surface(surfaceTexture);
        try {
            CaptureRequest.Builder captureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(previewSurface);
            captureRequestBuilder.addTarget(mImageReader.getSurface());
            captureRequestBuilder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, Range.create(0, Configuration.MAX_FPS_LIMIT));

            // tutaj startujemy sesję kamery
            OutputConfiguration outputConfiguration = new OutputConfiguration(previewSurface);
            OutputConfiguration imageReaderConfiguration = new OutputConfiguration(mImageReader.getSurface());
            SessionConfiguration sessionConfiguration = new SessionConfiguration(
                    SessionConfiguration.SESSION_REGULAR,
                    Arrays.asList(outputConfiguration, imageReaderConfiguration),
                    Runnable::run,
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            try {
                                session.setRepeatingRequest(captureRequestBuilder.build(), null, null);
                                detectorParam = new DetectorParam(
                                        mImageSize,
                                        SNKLivePreviewActivity.this,
                                        mImageView,
                                        mDetectorDelayTextView,
                                        mFaceParametersTextView,
                                        WorkingMode.LIVE
                                );
                                detectorParam.setCameraFacing(mCameraFacing);

                                Toast.makeText(getApplicationContext(), R.string.live_dtn_started_calibration_hint, Toast.LENGTH_LONG).show();
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            Toast.makeText(getApplicationContext(), R.string.live_dtn_camera_session_config_failed, Toast.LENGTH_SHORT).show();
                        }
                    }
            );
            mCameraDevice.createCaptureSession(sessionConfiguration);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
}