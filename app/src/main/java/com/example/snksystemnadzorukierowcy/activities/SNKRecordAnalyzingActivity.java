package com.example.snksystemnadzorukierowcy.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.hardware.camera2.CameraCharacteristics;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.DocumentsContract;
import android.provider.OpenableColumns;
import android.util.Size;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.snksystemnadzorukierowcy.Configuration;
import com.example.snksystemnadzorukierowcy.R;
import com.example.snksystemnadzorukierowcy.analyzers.SNKRecordCalibrationAnalyzer;
import com.example.snksystemnadzorukierowcy.detectors.DetectorParam;
import com.example.snksystemnadzorukierowcy.detectors.MainDetector;
import com.example.snksystemnadzorukierowcy.detectors.SNKDetector;
import com.example.snksystemnadzorukierowcy.utils.RecordParam;
import com.example.snksystemnadzorukierowcy.utils.WorkingMode;
import com.example.snksystemnadzorukierowcy.utils.file.CsvFormatter;

import java.io.IOException;

public class SNKRecordAnalyzingActivity extends AppCompatActivity {

    private static final int WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1234;
    // Request code for selecting a PDF document.
    private static final int PICK_MP4_FILE_REQUEST_CODE = 123;

    /**
     * Lokalizacja folderu z nagraniami SNK. Nie działa z <i>MediaMetadataRetriever</i> oraz <i>MediaExtractor</i>.
     */
    private final Uri SOURCE_DIRECTORY_LOCATION = Uri.parse("content://com.android.providers.media.documents/document/");
    /**
     * Nazwa pliku wideo, który wybraliśmy.
     */
    private String videoName;

    private Uri videoViewUri;

    /**
     * Obiekt, dzięki któremu pobieramy poszczególne metadane z nagrania.
     * W szczególności kolejne klatki z nagrania.
     */
    private final MediaMetadataRetriever mMediaMetadataRetriever = new MediaMetadataRetriever();
    /**
     * Obiekt, dzięki któremu jesteśmy w stanie pobrać te metadane z nagrania,
     * których nie możemy wydobyć za pomocą klasy MediaMetadataRetriever
     */
    private final MediaExtractor mMediaExtractor = new MediaExtractor();

    private final CsvFormatter csvFormatter = CsvFormatter.getInstance();

    /**
     * Widok, na którym wyświetlany jest podgląd.
     */
    private ImageView mImageView;

    private VideoView mVideoView;

    private Button mBrowseFilesButton;
    /**
     * Przycisk znajdujący się na dole ekranu.
     * Posłuży do wywoływania procesu kalibracji, a następnie do rozpoczęcia analizy nagrania.
     */
    private Button mBottomButton;
    private TextView analyzingProgressTextView;

    /**
     * Obiekt przechowujący podstawowe dane o analizowanym nagraniu.
     */
    private final RecordParam recordParam = new RecordParam();

    private HandlerThread mBackgroundHandlerThread;
    private Handler mBackgroundHandler;

    /**
     * Główny detektor, który zawiera w sobie wszystkie potrzebne detektory i decyduje o ich działaniu.
     */
    private final MainDetector detector = SNKDetector.getInstance();

    /**
     * Zmienna przechowujące wszystkie informacje, referencje potrzebne do poprawnej pracy detektora.
     */
    private DetectorParam detectorParam;

    /**
     * Listener rozpoczynający proces kalibracji nagrania.
     */
    private final View.OnClickListener onClickCalibrateRecordListener = v -> {
        mVideoView.pause();
        int currentPosition = mVideoView.getCurrentPosition(); //in millisecond
        int frameIndex = currentPosition / (recordParam.getFrameRate() * Configuration.RECORD_ANALYZING_FRAME_STEP);

        Bitmap bitmapFrame = mMediaMetadataRetriever.getFrameAtTime(
                currentPosition * 1000,
                MediaMetadataRetriever.OPTION_CLOSEST
        ); //unit in microsecond

        if (bitmapFrame == null) {
            Toast.makeText(getApplicationContext(), "Wystąpił błąd podczas odtwarzania klatki.", Toast.LENGTH_SHORT).show();
            mVideoView.start();
        } else {
            int videoWidth = bitmapFrame.getWidth();
            int videoHeight = bitmapFrame.getHeight();

            if (videoWidth >= 480 * 2 && videoHeight >= 360 * 2) {
                videoWidth /= 2;
                videoHeight /= 2;
            }

            recordParam.setWidth(videoWidth);
            recordParam.setHeight(videoHeight);

            Bitmap bitmap = Bitmap.createScaledBitmap(bitmapFrame, recordParam.getWidth(), recordParam.getHeight(), true);
            detectorParam.update(bitmap, 0);
            detector.process(detectorParam);

            mBottomButton.setVisibility(View.INVISIBLE);

            mBackgroundHandler.post(() -> {
                while(detector.getShouldWait()) {
                }
                csvFormatter.createCsvCalibrationResultFile(videoName.replace(".mp4", "") + "_calibrated_data", frameIndex, currentPosition);
            });

            mBackgroundHandler.post(analyzeRecord());

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_analyzing);

        SNKDetector.getInstance().setAnalyzer(SNKRecordCalibrationAnalyzer.getInstance());

        mBrowseFilesButton = findViewById(R.id.record_analyzing_browse_files_btn);
        mBrowseFilesButton.setOnClickListener(v -> openFile());

        mBottomButton = findViewById(R.id.record_analyzing_capture_frame_btn);

        mImageView = findViewById(R.id.record_analyzing_image_view);
        analyzingProgressTextView = findViewById(R.id.record_analyzing_progress_text_view);

        mVideoView = findViewById(R.id.videoView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
        }
        startBackgroundThread();
    }

    @Override
    protected void onPause() {
        stopBackgroundThread();
        super.onPause();
    }

    private void openFile() {
        mBrowseFilesButton.setVisibility(View.INVISIBLE);

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("video/mp4");

        // Optionally, specify a URI for the file that should appear in the
        // system file picker when it loads.
        intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, SOURCE_DIRECTORY_LOCATION);

        startActivityForResult(intent, PICK_MP4_FILE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_MP4_FILE_REQUEST_CODE) {
                videoViewUri = data.getData();
                videoName = getFileName(videoViewUri);
                prepareVideo();
            }
        }
    }

    /**
     * Zwraca liczbę klatek na sekundę z video.
     *
     * @param dataSource ścieżka dostępu do pliku video
     * @return liczbę FPS
     */
    private int extractFrameRate(String dataSource) {
        int frameRate = 25; // default value
        try {
            //Adjust data source as per the requirement if file, URI, etc.
            mMediaExtractor.setDataSource(dataSource);
            int numTracks = mMediaExtractor.getTrackCount();
            for (int i = 0; i < numTracks; ++i) {
                MediaFormat format = mMediaExtractor.getTrackFormat(i);
                String mime = format.getString(MediaFormat.KEY_MIME);
                if (mime.startsWith("video/")) {
                    if (format.containsKey(MediaFormat.KEY_FRAME_RATE)) {
                        frameRate = format.getInteger(MediaFormat.KEY_FRAME_RATE);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            mMediaExtractor.release();
        }

        return frameRate;
    }

    private void prepareVideoView() {
        mVideoView.setVideoURI(videoViewUri);

        mVideoView.setOnPreparedListener(mp -> {
            mBottomButton.setVisibility(View.VISIBLE);
            mVideoView.requestFocus();
            mVideoView.start();

            mBottomButton.setText("Kalibruj w tym momencie!");
            mBottomButton.setOnClickListener(onClickCalibrateRecordListener);

            Toast.makeText(getApplicationContext(), "Wybierz moment, na podstawie którego ma się dokonać kalibracja.", Toast.LENGTH_LONG).show();
        });
        mVideoView.setOnCompletionListener(mp -> mVideoView.start());
    }

    private void prepareVideo() {
        prepareVideoView();

        final String dataSource = "/storage/emulated/0/Movies/SNK/" + videoName;

        mMediaMetadataRetriever.setDataSource(dataSource);

        recordParam.setDurationInMillis(Integer.parseInt(mMediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)));
        recordParam.setFramesCount(Integer.parseInt(mMediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_FRAME_COUNT)));
        recordParam.setFrameRate(extractFrameRate(dataSource) / Configuration.RECORD_ANALYZING_FRAME_STEP);

        detectorParam = new DetectorParam(
                new Size(recordParam.getWidth(), recordParam.getHeight()),
                SNKRecordAnalyzingActivity.this,
                mImageView,
                findViewById(R.id.current_face_parameters_text_view),
                WorkingMode.RECORD
        );
        detectorParam.setCameraFacing(CameraCharacteristics.LENS_FACING_EXTERNAL);
        detectorParam.setFrameRate(recordParam.getFrameRate());
    }

    private Runnable analyzeRecord() {
        return () -> {
            while (true) {
                if (detector.getShouldWait()) {
                    continue;
                }

                final int frameIndex = recordParam.getCurrentFrameIndex();

                runOnUiThread(() -> {
                            int timeProcessed = frameIndex * recordParam.getFrameRate() * Configuration.RECORD_ANALYZING_FRAME_STEP;
                            analyzingProgressTextView.setText(timeProcessed + "ms / " + recordParam.getDurationInMillis() + "ms (" + frameIndex * 100 / recordParam.getFramesCount() + "%)");
                        }
                );
                if (frameIndex >= recordParam.getFramesCount()) {
                    finishAnalyze();
                    break;
                }

                MediaMetadataRetriever.BitmapParams params = new MediaMetadataRetriever.BitmapParams();
                params.setPreferredConfig(Bitmap.Config.ARGB_8888);

                final Bitmap largeBitmap = mMediaMetadataRetriever.getFrameAtIndex(frameIndex, params);

                final Bitmap bitmap = Bitmap.createScaledBitmap(largeBitmap, recordParam.getWidth(), recordParam.getHeight(), true);

                detectorParam.update(bitmap, 0);

                detector.process(detectorParam);

                recordParam.setCurrentFrameIndex(frameIndex + Configuration.RECORD_ANALYZING_FRAME_STEP);
            }

            csvFormatter.createCsvFile(videoName.replace(".mp4", "") + "_REPORT");
        };
    }

    private String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private void startBackgroundThread() {
        mBackgroundHandlerThread = new HandlerThread("recordAnalyzing");
        mBackgroundHandlerThread.start();
        mBackgroundHandler = new Handler(mBackgroundHandlerThread.getLooper());
    }

    private void stopBackgroundThread() {
        mBackgroundHandlerThread.quitSafely();
        try {
            mBackgroundHandlerThread.join();
            mBackgroundHandlerThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void finishAnalyze() {
        mMediaMetadataRetriever.release();
        runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Koniec nagrania.", Toast.LENGTH_SHORT).show());
        startActivity(new Intent(getApplicationContext(), ChooserActivity.class));
    }
}