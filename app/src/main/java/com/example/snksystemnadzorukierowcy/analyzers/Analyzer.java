package com.example.snksystemnadzorukierowcy.analyzers;

public interface Analyzer {
    void analyze(SNKAnalyzerParam param);
}
