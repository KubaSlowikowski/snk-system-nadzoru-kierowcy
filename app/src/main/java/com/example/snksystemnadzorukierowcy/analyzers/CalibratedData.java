package com.example.snksystemnadzorukierowcy.analyzers;

/**
 * Klasa przechowująca dane po kalibracji urządzenia
 */
public class CalibratedData {

    private static CalibratedData instance;

    /**
     * Skalibrowany średni kąt rotacji głowy wokół osi X.
     */
    private float avgHeadEulerAngleX;
    /**
     * Skalibrowany średni kąt rotacji głowy wokół osi Y.
     */
    private float avgHeadEulerAngleY;
    /**
     * Skalibrowany średni kąt rotacji głowy wokół osi Z.
     */
    private float avgHeadEulerAngleZ;

    private CalibratedData() {
    }

    public static CalibratedData getInstance() {
        if (instance == null) {
            instance = new CalibratedData();
        }
        return instance;
    }

    public float getAvgHeadEulerAngleX() {
        return avgHeadEulerAngleX;
    }

    public void setAvgHeadEulerAngleX(float avgHeadEulerAngleX) {
        this.avgHeadEulerAngleX = avgHeadEulerAngleX;
    }

    public float getAvgHeadEulerAngleY() {
        return avgHeadEulerAngleY;
    }

    public void setAvgHeadEulerAngleY(float avgHeadEulerAngleY) {
        this.avgHeadEulerAngleY = avgHeadEulerAngleY;
    }

    public float getAvgHeadEulerAngleZ() {
        return avgHeadEulerAngleZ;
    }

    public void setAvgHeadEulerAngleZ(float avgHeadEulerAngleZ) {
        this.avgHeadEulerAngleZ = avgHeadEulerAngleZ;
    }
}