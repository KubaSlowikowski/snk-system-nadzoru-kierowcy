package com.example.snksystemnadzorukierowcy.analyzers;

import com.google.mlkit.vision.face.Face;

public interface Calibrator {
    void calibrate(Face face);
}
