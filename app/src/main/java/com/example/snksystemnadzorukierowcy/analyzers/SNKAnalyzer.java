package com.example.snksystemnadzorukierowcy.analyzers;

import android.widget.TextView;

import com.example.snksystemnadzorukierowcy.Configuration;
import com.example.snksystemnadzorukierowcy.R;
import com.example.snksystemnadzorukierowcy.analyzers.guards.Guard;
import com.example.snksystemnadzorukierowcy.analyzers.guards.GuardParam;
import com.example.snksystemnadzorukierowcy.analyzers.guards.LiveModeGuard;
import com.example.snksystemnadzorukierowcy.analyzers.guards.RecordModeGuard;
import com.example.snksystemnadzorukierowcy.classifiers.FaceClassifier;
import com.example.snksystemnadzorukierowcy.classifiers.PoseClassificationResult;
import com.example.snksystemnadzorukierowcy.classifiers.PoseClassifier;
import com.example.snksystemnadzorukierowcy.utils.NumberUtils;
import com.example.snksystemnadzorukierowcy.utils.WorkingMode;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.pose.Pose;

public class SNKAnalyzer implements Analyzer {

    private static SNKAnalyzer instance;

    private final float avg_x = CalibratedData.getInstance().getAvgHeadEulerAngleX();
    private final float avg_y = CalibratedData.getInstance().getAvgHeadEulerAngleY();
    private final float avg_z = CalibratedData.getInstance().getAvgHeadEulerAngleZ();

    private TextView calibrationParamsTextView;
    private TextView faceParamsTextView;
    private TextView poseParamsTextView;

    private Guard guard;
    private GuardParam guardParam; // guardParam został dodany tylko w celu utworzenia raportu .CSV z testów, nie służy on w pracy w trybie rzeczywistym.

    private SNKAnalyzer() {
    }

    public static SNKAnalyzer getInstance() {
        if (instance == null) {
            instance = new SNKAnalyzer();
        }
        return instance;
    }

    /**
     * Inicjalizuje niektóre pola w klasie analizatora.
     *
     * @param param Parametr analizatora
     */
    private void init(SNKAnalyzerParam param) {
        calibrationParamsTextView = param.getActivity().findViewById(R.id.calibration_parameters_text_view);
        StringBuilder sb = new StringBuilder();
        sb
                .append("avgX: ").append(NumberUtils.round(avg_x)).append("[deg]\n")
                .append("avgY: ").append(NumberUtils.round(avg_y)).append("[deg]\n")
                .append("avgZ: ").append(NumberUtils.round(avg_z)).append("[deg]\n");
        calibrationParamsTextView.setText(sb.toString());

        faceParamsTextView = param.getActivity().findViewById(R.id.current_face_parameters_text_view);

        poseParamsTextView = param.getActivity().findViewById(R.id.pose_params_text_view);

        if (param.getWorkingMode() == WorkingMode.LIVE) {
            guard = new LiveModeGuard();
        } else {
            guard = new RecordModeGuard(param.getFrameRate());
        }
        PoseClassifier.setGuard(guard);
    }

    @Override
    public void analyze(SNKAnalyzerParam param) {
        if (calibrationParamsTextView == null || faceParamsTextView == null) {
            init(param);
        }

        guardParam = new GuardParam();

        if (param.getFace() != null) {
            analyzeFace(param.getFace());
            analyzePose(param.getPose());
        } else {
            guard.warn_face_not_present();
        }

        if (guard instanceof RecordModeGuard) {
            ((RecordModeGuard) guard).updateDetectionResults(guardParam);
        }

        guard.checkWarnings();
    }

    private void analyzeFace(Face face) {
        guard.face_presence_OK();
        drawFaceParameters(face);
        FaceClassifier.analyzeHeadRotation(face, guard, guardParam);
        FaceClassifier.analyzeEyes(face, guard, guardParam);
    }

    private void analyzePose(Pose pose) {
        if (pose == null) {
            return;
        }

        poseParamsTextView.setText("Pose params: ");

        final PoseClassificationResult result = new PoseClassificationResult();

        PoseClassifier.calculateLandmarksDistances(result, pose);
        PoseClassifier.calculateArmAngles(result, pose);
        PoseClassifier.classifyPose(result, guardParam);

        displayPoseParams(result, poseParamsTextView);
    }

    private void displayPoseParams(PoseClassificationResult result, TextView poseParamsTextView) {
        StringBuilder stringBuilder = new StringBuilder();

        if (result.getLeftElbowAngle() != Integer.MAX_VALUE && result.getLeftHandToFaceDistance() != Float.MAX_VALUE) {
            stringBuilder
                    .append(" \n")
                    .append("Left elbow angle: ").append(result.getLeftElbowAngle()).append(" [deg] \n")
                    .append("lHand2Nose Ratio: ").append(NumberUtils.round(result.getLeftHandToFaceDistance() / result.getEarToEarDistance()));
        }

        if (result.getRightElbowAngle() != Integer.MAX_VALUE && result.getRightHandToFaceDistance() != Float.MAX_VALUE) {
            stringBuilder
                    .append(" \n")
                    .append("Right elbow angle ").append(result.getRightElbowAngle()).append(" [deg] \n")
                    .append("rHand2Nose Ratio: ").append(NumberUtils.round(result.getRightHandToFaceDistance() / result.getEarToEarDistance()));
        }

        poseParamsTextView.setText(poseParamsTextView.getText() + stringBuilder.toString());
    }

    /**
     * Zamieszcza na UI informacje m.in. kątach  rotacji twarzy,
     * prawdopodobieństwie otwarcia oczu.
     *
     * @param face Wykryta twarz
     */
    public void drawFaceParameters(Face face) {
        StringBuilder stringBuilder = new StringBuilder();

        addEulerAngles(stringBuilder, face);
        addProbabilities(stringBuilder, face);

        faceParamsTextView.setText(stringBuilder.toString());
    }

    private void addEulerAngles(StringBuilder stringBuilder, Face face) {
        float x = face.getHeadEulerAngleX();
        float y = face.getHeadEulerAngleY();
        float z = face.getHeadEulerAngleZ();
        stringBuilder
                .append("X: ").append(NumberUtils.round(x)).append(" [deg]\n")
                .append("Y: ").append(NumberUtils.round(y)).append(" [deg]\n")
                .append("Z: ").append(NumberUtils.round(z)).append(" [deg]\n");
    }

    private void addProbabilities(StringBuilder stringBuilder, Face face) {
        int rightEyeOpenProb, leftEyeOpenProb, smilingProb;

        if (face.getRightEyeOpenProbability() != null && face.getLeftEyeOpenProbability() != null) {
            rightEyeOpenProb = (int) (face.getRightEyeOpenProbability() * 100);
            leftEyeOpenProb = (int) (face.getLeftEyeOpenProbability() * 100);
            stringBuilder
                    .append("Right eye open: ").append(rightEyeOpenProb).append("% \n")
                    .append("Left eye open: ").append(leftEyeOpenProb).append("% \n");
        }

        if (face.getSmilingProbability() != null) {
            smilingProb = (int) (face.getSmilingProbability() * 100);
            stringBuilder
                    .append("Smiling: ").append(smilingProb).append("% \n");
        }
    }
}