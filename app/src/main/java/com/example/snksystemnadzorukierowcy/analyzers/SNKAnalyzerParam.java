package com.example.snksystemnadzorukierowcy.analyzers;

import android.app.Activity;

import com.example.snksystemnadzorukierowcy.utils.WorkingMode;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.pose.Pose;

public class SNKAnalyzerParam {
    private Face face;
    private Pose pose;
    private final Activity activity;
    private int frameRate;
    private WorkingMode workingMode;

    public SNKAnalyzerParam(Activity activity) {
        this.activity = activity;
    }

    public Face getFace() {
        return face;
    }

    public void setFace(Face face) {
        this.face = face;
    }

    public Pose getPose() {
        return pose;
    }

    public void setPose(Pose pose) {
        this.pose = pose;
    }

    public Activity getActivity() {
        return activity;
    }

    public int getFrameRate() {
        return frameRate;
    }

    public void setFrameRate(int frameRate) {
        this.frameRate = frameRate;
    }

    public WorkingMode getWorkingMode() {
        return workingMode;
    }

    public void setWorkingMode(WorkingMode workingMode) {
        this.workingMode = workingMode;
    }
}
