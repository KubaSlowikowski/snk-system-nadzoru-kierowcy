package com.example.snksystemnadzorukierowcy.analyzers;

import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.snksystemnadzorukierowcy.Configuration;
import com.example.snksystemnadzorukierowcy.R;
import com.example.snksystemnadzorukierowcy.detectors.SNKDetector;
import com.example.snksystemnadzorukierowcy.utils.SoundUtils;
import com.google.mlkit.vision.face.Face;

public class SNKCalibrationAnalyzer implements Calibrator, Analyzer {

    private static SNKCalibrationAnalyzer instance;

    private short iterationCounter = 0;

    private final CalibratedData calibratedData = CalibratedData.getInstance();

    private TextView countDownTextView;

    private TextView countDownInfoTextView;


    private boolean timerStarted = false;
    private final CountDownTimer countDownTimer = new CountDownTimer(Configuration.CALIBRATION_TIME * 1000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            SoundUtils.beep();
            countDownTextView.setText(String.valueOf(millisUntilFinished / 1000));
        }

        @Override
        public void onFinish() {
            SoundUtils.doubleBeep();
            countDownInfoTextView.setText("");
            countDownTextView.setText("");
            Log.d("SNKCalibrationAnalyzer", "Calibration finished.");
            SNKDetector.getInstance().setAnalyzer(SNKAnalyzer.getInstance());
        }
    };

    private SNKCalibrationAnalyzer() {
    }

    public static SNKCalibrationAnalyzer getInstance() {
        if (instance == null) {
            instance = new SNKCalibrationAnalyzer();
        }
        return instance;
    }

    @Override
    public void analyze(SNKAnalyzerParam param) {
        if (!timerStarted) {
            countDownTextView = param.getActivity().findViewById(R.id.live_dtn_calibration_number_in_center_text_view);
            countDownInfoTextView = param.getActivity().findViewById(R.id.live_dtn_calibration_text_above_center_text_view);

            countDownTextView.setVisibility(View.VISIBLE);
            countDownInfoTextView.setVisibility(View.VISIBLE);

            countDownInfoTextView.setText(R.string.calibration_countdown_info);

            timerStarted = true;
            countDownTimer.start();
        }

        if (param.getFace() != null) {
            calibrate(param.getFace());
        }
    }

    @Override
    public void calibrate(Face face) {
        Log.d("calibrating", "calibrating");

        iterationCounter++;

        float avgX = calibratedData.getAvgHeadEulerAngleX();
        avgX = (avgX * (iterationCounter - 1) + face.getHeadEulerAngleX()) / iterationCounter;

        calibratedData.setAvgHeadEulerAngleX(avgX);

        float avgY = calibratedData.getAvgHeadEulerAngleY();
        avgY = (avgY * (iterationCounter - 1) + face.getHeadEulerAngleY()) / iterationCounter;

        calibratedData.setAvgHeadEulerAngleY(avgY);

        float avgZ = calibratedData.getAvgHeadEulerAngleZ();
        avgZ = (avgZ * (iterationCounter - 1) + face.getHeadEulerAngleZ()) / iterationCounter;

        calibratedData.setAvgHeadEulerAngleZ(avgZ);
    }
}
