package com.example.snksystemnadzorukierowcy.analyzers;

import com.example.snksystemnadzorukierowcy.detectors.SNKDetector;
import com.google.mlkit.vision.face.Face;

public class SNKRecordCalibrationAnalyzer implements Calibrator, Analyzer {
    private static SNKRecordCalibrationAnalyzer instance;

    private final CalibratedData calibratedData = CalibratedData.getInstance();

    private SNKRecordCalibrationAnalyzer() {}

    public static SNKRecordCalibrationAnalyzer getInstance() {
        if (instance == null) {
            instance = new SNKRecordCalibrationAnalyzer();
        }
        return instance;
    }

    @Override
    public void analyze(SNKAnalyzerParam param) {
        if (param.getFace() != null) {
            calibrate(param.getFace());
        }
    }

    @Override
    public void calibrate(Face face) {
        float avgX = face.getHeadEulerAngleX();
        float avgY = face.getHeadEulerAngleY();
        float avgZ = face.getHeadEulerAngleZ();

        calibratedData.setAvgHeadEulerAngleX(avgX);
        calibratedData.setAvgHeadEulerAngleY(avgY);
        calibratedData.setAvgHeadEulerAngleZ(avgZ);

        SNKDetector.getInstance().setAnalyzer(SNKAnalyzer.getInstance());
    }
}
