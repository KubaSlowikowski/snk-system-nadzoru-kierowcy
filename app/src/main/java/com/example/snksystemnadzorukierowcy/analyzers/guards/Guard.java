package com.example.snksystemnadzorukierowcy.analyzers.guards;

public interface Guard {

    void warn_head_tilt_x();

    void warn_head_tilt_y();

    void warn_head_tilt_z();

    void head_tilt_x_OK();

    void head_tilt_y_OK();

    void head_tilt_z_OK();

    void warn_eyes_closed();

    void eyes_open_OK();

    void warn_face_not_present();

    void face_presence_OK();

    void warn_left_hand_near_head_present();

    void left_hand_OK();

    void warn_right_hand_near_head_present();

    void right_hand_OK();

    /**
     * Sprawdzanie, czy któryś z poziomu ostrzeżeń nie przekroczył normy.
     */
    void checkWarnings();
}
