package com.example.snksystemnadzorukierowcy.analyzers.guards;

/**
 * Klasa która zawiera wszystkie wyjściowe parametry z detektorów, tj. kąty rotacji głowy, pradopodobieństwo zamknięcia oczy itd.
 */
public class GuardParam {

    /**
     * Odchylenie głowy o kąt Eulera X względem skalibrowanych danych.
     */
    private float headXAngle = -1000;
    /**
     * Odchylenie głowy o kąt Eulera Y względem skalibrowanych danych.
     */
    private float headYAngle = -1000;
    /**
     * Odchylenie głowy o kąt Eulera Z względem skalibrowanych danych.
     */
    private float headZAngle = -1000;

    private float eyesOpenProbability = -100;
    private float smilingProbability = -100;

    private int leftElbowAngle = 1000;
    private int rightElbowAngle = 1000;

    /**
     * Współczynnik odległości lewa dłoń-głowa a lewe ucho-prawe ucho.
     */
    private float leftHandNearHeadRatio = 1000;
    /**
     * Współczynnik odległości prawa dłoń-głowa a lewe ucho-prawe ucho.
     */
    private float rightHandNearHeadRatio = 1000;

    public GuardParam() {
    }

    public float getHeadXAngle() {
        return headXAngle;
    }

    public void setHeadXAngle(float headXAngle) {
        this.headXAngle = headXAngle;
    }

    public float getHeadYAngle() {
        return headYAngle;
    }

    public void setHeadYAngle(float headYAngle) {
        this.headYAngle = headYAngle;
    }

    public float getHeadZAngle() {
        return headZAngle;
    }

    public void setHeadZAngle(float headZAngle) {
        this.headZAngle = headZAngle;
    }

    public float getEyesOpenProbability() {
        return eyesOpenProbability;
    }

    public void setEyesOpenProbability(float eyesOpenProbability) {
        this.eyesOpenProbability = eyesOpenProbability;
    }

    public int getLeftElbowAngle() {
        return leftElbowAngle;
    }

    public void setLeftElbowAngle(int leftElbowAngle) {
        this.leftElbowAngle = leftElbowAngle;
    }

    public int getRightElbowAngle() {
        return rightElbowAngle;
    }

    public void setRightElbowAngle(int rightElbowAngle) {
        this.rightElbowAngle = rightElbowAngle;
    }

    public float getLeftHandNearHeadRatio() {
        return leftHandNearHeadRatio;
    }

    public void setLeftHandNearHeadRatio(float leftHandNearHeadRatio) {
        this.leftHandNearHeadRatio = leftHandNearHeadRatio;
    }

    public float getRightHandNearHeadRatio() {
        return rightHandNearHeadRatio;
    }

    public void setRightHandNearHeadRatio(float rightHandNearHeadRatio) {
        this.rightHandNearHeadRatio = rightHandNearHeadRatio;
    }

    public float getSmilingProbability() {
        return smilingProbability;
    }

    public void setSmilingProbability(float smilingProbability) {
        this.smilingProbability = smilingProbability;
    }
}
