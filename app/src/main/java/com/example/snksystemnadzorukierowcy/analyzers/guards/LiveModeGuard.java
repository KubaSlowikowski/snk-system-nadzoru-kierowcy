package com.example.snksystemnadzorukierowcy.analyzers.guards;

import com.example.snksystemnadzorukierowcy.Configuration;
import com.example.snksystemnadzorukierowcy.utils.SoundUtils;

import java.time.Duration;
import java.time.Instant;

/**
 * Klasa, która gromadzi poziomy ostrzeżeń i alarmuje w przypadku przekroczenia normy.
 */
public class LiveModeGuard implements Guard {

    /**
     * Moment, od którego algorytm widzi zbyt duże pochylenie głowy (kąt eulera X).
     */
    private Instant warning_head_tilt_x_start;
    /**
     * Moment, od którego algorytm widzi zbyt duże pochylenie głowy (kąt eulera Y).
     */
    private Instant warning_head_tilt_y_start;
    /**
     * Moment, od którego algorytm widzi zbyt duże pochylenie głowy (kąt eulera Z).
     */
    private Instant warning_head_tilt_z_start;

    /**
     * Moment, od którego algorytm widzi prawidłowe pochylenie głowy (kąt eulera X).
     * Zmienna ta jest potrzebna do prawidłowego obniżania poziomu ostrzeżeń.
     */
    private Instant head_tilt_x_OK_start;
    /**
     * Moment, od którego algorytm widzi prawidłowe pochylenie głowy (kąt eulera Y).
     * Zmienna ta jest potrzebna do prawidłowego obniżania poziomu ostrzeżeń.
     */
    private Instant head_tilt_y_OK_start;
    /**
     * Moment, od którego algorytm widzi prawidłowe pochylenie głowy (kąt eulera Z).
     * Zmienna ta jest potrzebna do prawidłowego obniżania poziomu ostrzeżeń.
     */
    private Instant head_tilt_z_OK_start;

    /**
     * Moment, od którego algorytm widzi zamknięte oczy kierowcy.
     */
    private Instant warning_eyes_closed_start;
    /**
     * Moment, od którego algorytm widzi otwarte oczy kierowcy.
     * Zmienna ta jest potrzebna do prawidłowego obniżania poziomu ostrzeżeń.
     */
    private Instant eyes_OK_start;

    /**
     * Moment, od którego kierowca zniknął z pola widzenia, przestał być wykrywany.
     */
    private Instant warning_face_not_present_start;
    /**
     * Moment, od którego algorytm widzi kierowcę.
     * Zmienna ta jest potrzebna do prawidłowego obniżania poziomu ostrzeżeń.
     */
    private Instant face_presence_OK_start;

    /**
     * Moment, od którego algorytm zaczyna wykrywać lewą dłoń przy twarzy.
     */
    private Instant warning_left_hand_near_head_start;
    /**
     * Moment, od którego algorytm zaczyna wykrywać prawą dłoń przy twarzy.
     */
    private Instant warning_right_hand_near_head_start;

    /**
     * Moment, od którego algorytm stwierdza, że lewa ręka jest poprawnie ułożona.
     * Zmienna ta jest potrzebna do prawidłowego obniżania poziomu ostrzeżeń.
     */
    private Instant left_hand_OK_start;
    /**
     * Moment, od którego algorytm stwierdza, że prawa ręka jest poprawnie ułożona.
     * Zmienna ta jest potrzebna do prawidłowego obniżania poziomu ostrzeżeń.
     */
    private Instant right_hand_OK_start;

    @Override
    public void warn_head_tilt_x() {
        if (warning_head_tilt_x_start == null) {
            warning_head_tilt_x_start = Instant.now();
        } else {
            if (Duration.between(warning_head_tilt_x_start, Instant.now()).getSeconds() >= Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME) {
                head_tilt_x_OK_start = null;
            }
        }
    }

    @Override
    public void warn_head_tilt_y() {
        if (warning_head_tilt_y_start == null) {
            warning_head_tilt_y_start = Instant.now();
        } else {
            if (Duration.between(warning_head_tilt_y_start, Instant.now()).getSeconds() >= Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME) {
                head_tilt_y_OK_start = null;
            }
        }
    }

    @Override
    public void warn_head_tilt_z() {
        if (warning_head_tilt_z_start == null) {
            warning_head_tilt_z_start = Instant.now();
        } else {
            if (Duration.between(warning_head_tilt_z_start, Instant.now()).getSeconds() >= Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME) {
                head_tilt_z_OK_start = null;
            }
        }
    }

    @Override
    public void head_tilt_x_OK() {
        if (head_tilt_x_OK_start == null) {
            head_tilt_x_OK_start = Instant.now();
        } else {
            if (Duration.between(head_tilt_x_OK_start, Instant.now()).getSeconds() >= Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME) {
                warning_head_tilt_x_start = null;
            }
        }
    }

    @Override
    public void head_tilt_y_OK() {
        if (head_tilt_y_OK_start == null) {
            head_tilt_y_OK_start = Instant.now();
        } else {
            if (Duration.between(head_tilt_y_OK_start, Instant.now()).getSeconds() >= Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME) {
                warning_head_tilt_y_start = null;
            }
        }
    }

    @Override
    public void head_tilt_z_OK() {
        if (head_tilt_z_OK_start == null) {
            head_tilt_z_OK_start = Instant.now();
        } else {
            if (Duration.between(head_tilt_z_OK_start, Instant.now()).getSeconds() >= Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME) {
                warning_head_tilt_z_start = null;
            }
        }
    }

    @Override
    public void warn_eyes_closed() {
        if (warning_eyes_closed_start == null) {
            warning_eyes_closed_start = Instant.now();
        } else {
            if (Duration.between(warning_eyes_closed_start, Instant.now()).getSeconds() * 1000 >= Configuration.GUARD_EYES_CLOSED_WARNING_TURN_OFF_TIME * 1000) {
                eyes_OK_start = null;
            }
        }
    }

    @Override
    public void eyes_open_OK() {
        if (eyes_OK_start == null) {
            eyes_OK_start = Instant.now();
        } else {
            if (Duration.between(eyes_OK_start, Instant.now()).toMillis() >= Configuration.GUARD_EYES_CLOSED_WARNING_TURN_OFF_TIME * 1000) {
                warning_eyes_closed_start = null;
            }
        }
    }

    @Override
    public void warn_face_not_present() {
        if (warning_face_not_present_start == null) {
            warning_face_not_present_start = Instant.now();
        } else {
            if (Duration.between(warning_face_not_present_start, Instant.now()).getSeconds() >= Configuration.WARNING_DURATION_FACE_NOT_PRESENT) {
                face_presence_OK_start = null;
            }
        }
    }

    @Override
    public void face_presence_OK() {
        if (face_presence_OK_start == null) {
            face_presence_OK_start = Instant.now();
        } else {
            if (Duration.between(face_presence_OK_start, Instant.now()).getSeconds() >= Configuration.GUARD_FACE_NOT_PRESENT_WARNING_TURN_OFF_TIME) {
                warning_face_not_present_start = null;
            }
        }
    }

    @Override
    public void warn_left_hand_near_head_present() {
        if (warning_left_hand_near_head_start == null) {
            warning_left_hand_near_head_start = Instant.now();
        } else {
            if (Duration.between(warning_left_hand_near_head_start, Instant.now()).getSeconds() >= Configuration.WARNING_DURATION_HAND_NEAR_HEAD) {
                left_hand_OK_start = null;
            }
        }
    }

    @Override
    public void left_hand_OK() {
        if (left_hand_OK_start == null) {
            left_hand_OK_start = Instant.now();
        } else {
            if (Duration.between(left_hand_OK_start, Instant.now()).getSeconds() >= Configuration.GUARD_HAND_NEAR_HEAD_TURN_OFF_TIME) {
                warning_left_hand_near_head_start = null;
            }
        }
    }

    @Override
    public void warn_right_hand_near_head_present() {
        if (warning_right_hand_near_head_start == null) {
            warning_right_hand_near_head_start = Instant.now();
        } else {
            if (Duration.between(warning_right_hand_near_head_start, Instant.now()).getSeconds() >= Configuration.WARNING_DURATION_HAND_NEAR_HEAD) {
                right_hand_OK_start = null;
            }
        }
    }

    @Override
    public void right_hand_OK() {
        if (right_hand_OK_start == null) {
            right_hand_OK_start = Instant.now();
        } else {
            if (Duration.between(right_hand_OK_start, Instant.now()).getSeconds() >= Configuration.GUARD_HAND_NEAR_HEAD_TURN_OFF_TIME) {
                warning_right_hand_near_head_start = null;
            }
        }
    }

    /**
     * Sprawdzanie, czy któryś z poziomu ostrzeżeń nie przekroczył normy.
     */
    @Override
    public void checkWarnings() {
        if (
                shouldRaiseAlert(warning_head_tilt_x_start, Configuration.WARNING_DURATION_HEAD_TILT) ||
                        shouldRaiseAlert(warning_head_tilt_y_start, Configuration.WARNING_DURATION_HEAD_TILT) ||
                        shouldRaiseAlert(warning_head_tilt_z_start, Configuration.WARNING_DURATION_HEAD_TILT) ||
                        shouldRaiseAlert(warning_eyes_closed_start, Configuration.WARNING_DURATION_EYES_CLOSED) ||
                        shouldRaiseAlert(warning_face_not_present_start, Configuration.WARNING_DURATION_FACE_NOT_PRESENT) ||
                        shouldRaiseAlert(warning_left_hand_near_head_start, Configuration.WARNING_DURATION_HAND_NEAR_HEAD) ||
                        shouldRaiseAlert(warning_right_hand_near_head_start, Configuration.WARNING_DURATION_HAND_NEAR_HEAD)
        ) {
            SoundUtils.alert();
        }
    }

    /**
     * Sprawdza czy ostrzeżenie przekroczyło próg wywołania alarmu.
     *
     * @param warningDuration Czas trwania ostrzeżenia.
     * @param alertThreshold  Ilość sekund, po których ma się włączyć alarm.
     * @return true - jeśli trzeba włączyć alarm
     */
    public boolean shouldRaiseAlert(Instant warningDuration, int alertThreshold) {
        if (warningDuration == null) {
            return false;
        }
        return Duration.between(warningDuration, Instant.now()).getSeconds() >= alertThreshold;
    }
}