package com.example.snksystemnadzorukierowcy.analyzers.guards;

import com.example.snksystemnadzorukierowcy.Configuration;
import com.example.snksystemnadzorukierowcy.utils.NumberUtils;
import com.example.snksystemnadzorukierowcy.utils.file.CsvFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class RecordModeGuard implements Guard {

    enum Warning {
        HEAD_TILT_X,
        HEAD_TILT_Y,
        HEAD_TILT_Z,
        FACE_NOT_PRESENT,
        EYES_CLOSED,
        LEFT_HAND_NEAR_HEAD,
        RIGHT_HAND_NEAR_HEAD
    }

    private final int STEP;
    private int frameIndex;
    private int timeElapsedInMillis;

    private final CsvFormatter csvFormatter = CsvFormatter.getInstance();

    private final AtomicInteger warning_head_tilt_x = new AtomicInteger(0);
    private final AtomicInteger warning_head_tilt_y = new AtomicInteger(0);
    private final AtomicInteger warning_head_tilt_z = new AtomicInteger(0);

    private final AtomicInteger head_tilt_x_OK = new AtomicInteger(0);
    private final AtomicInteger head_tilt_y_OK = new AtomicInteger(0);
    private final AtomicInteger head_tilt_z_OK = new AtomicInteger(0);

    private final AtomicInteger warning_eyes_closed = new AtomicInteger(0);
    private final AtomicInteger eyes_OK = new AtomicInteger(0);

    private final AtomicInteger warning_face_not_present = new AtomicInteger(0);
    private final AtomicInteger face_presence_OK = new AtomicInteger(0);

    private final AtomicInteger warning_left_hand_near_head = new AtomicInteger(0);
    private final AtomicInteger left_hand_OK = new AtomicInteger(0);

    private final AtomicInteger warning_right_hand_near_head = new AtomicInteger(0);
    private final AtomicInteger right_hand_OK = new AtomicInteger(0);

    private final ArrayList<AtomicInteger> times = new ArrayList<>();

    private final Map<Warning, Boolean> alerts = new HashMap<>();

    private GuardParam guardParam;

    public RecordModeGuard(int frameRate) {
        this.STEP = 1000 / frameRate;

        alerts.put(Warning.HEAD_TILT_X, false);
        alerts.put(Warning.HEAD_TILT_Y, false);
        alerts.put(Warning.HEAD_TILT_Z, false);
        alerts.put(Warning.EYES_CLOSED, false);
        alerts.put(Warning.FACE_NOT_PRESENT, false);
        alerts.put(Warning.LEFT_HAND_NEAR_HEAD, false);
        alerts.put(Warning.RIGHT_HAND_NEAR_HEAD, false);

        times.clear();
    }

    private void processWarningTime(AtomicInteger warning_time, AtomicInteger ok_time, int turnOffTimeInMillis) {
        processWarningTime(warning_time, ok_time, (float) turnOffTimeInMillis);
    }

    private void processWarningTime(AtomicInteger warning_time, AtomicInteger ok_time, float turnOffTimeInSeconds) {
        if (!times.contains(warning_time)) {
            times.add(warning_time);
        }
        if (warning_time.get() - (turnOffTimeInSeconds * 1000) >= 0) {
            ok_time.set(0);
            times.remove(ok_time);
        }
    }

    private void processOkTime(AtomicInteger warning_time, AtomicInteger ok_time, int turnOffTimeInMillis) {
        processOkTime(warning_time, ok_time, (float) turnOffTimeInMillis);
    }

    private void processOkTime(AtomicInteger warning_time, AtomicInteger ok_time, float turnOffTimeInMillis) {
        if (!times.contains(ok_time)) {
            times.add(ok_time);
        }
        if (ok_time.get() - (turnOffTimeInMillis * 1000) >= 0) {
            warning_time.set(0);
            times.remove(warning_time);
        }
    }

    @Override
    public void warn_head_tilt_x() {
        processWarningTime(warning_head_tilt_x, head_tilt_x_OK, Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME);
    }

    @Override
    public void warn_head_tilt_y() {
        processWarningTime(warning_head_tilt_y, head_tilt_y_OK, Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME);
    }

    @Override
    public void warn_head_tilt_z() {
        processWarningTime(warning_head_tilt_z, head_tilt_z_OK, Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME);
    }

    @Override
    public void head_tilt_x_OK() {
        processOkTime(warning_head_tilt_x, head_tilt_x_OK, Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME);
    }

    @Override
    public void head_tilt_y_OK() {
        processOkTime(warning_head_tilt_y, head_tilt_y_OK, Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME);
    }

    @Override
    public void head_tilt_z_OK() {
        processOkTime(warning_head_tilt_z, head_tilt_z_OK, Configuration.GUARD_HEAD_TILT_WARNING_TURN_OFF_TIME);
    }

    @Override
    public void warn_eyes_closed() {
        processWarningTime(warning_eyes_closed, eyes_OK, Configuration.GUARD_EYES_CLOSED_WARNING_TURN_OFF_TIME);
    }

    @Override
    public void eyes_open_OK() {
        processOkTime(warning_eyes_closed, eyes_OK, Configuration.GUARD_EYES_CLOSED_WARNING_TURN_OFF_TIME);
    }

    @Override
    public void warn_face_not_present() {
        processWarningTime(warning_face_not_present, face_presence_OK, Configuration.WARNING_DURATION_FACE_NOT_PRESENT);
    }

    @Override
    public void face_presence_OK() {
        processOkTime(warning_face_not_present, face_presence_OK, Configuration.WARNING_DURATION_FACE_NOT_PRESENT);
    }

    @Override
    public void warn_left_hand_near_head_present() {
        processWarningTime(warning_left_hand_near_head, left_hand_OK, Configuration.GUARD_HAND_NEAR_HEAD_TURN_OFF_TIME);
    }

    @Override
    public void left_hand_OK() {
        processOkTime(warning_left_hand_near_head, left_hand_OK, Configuration.GUARD_HAND_NEAR_HEAD_TURN_OFF_TIME);
    }

    @Override
    public void warn_right_hand_near_head_present() {
        processWarningTime(warning_right_hand_near_head, right_hand_OK, Configuration.GUARD_HAND_NEAR_HEAD_TURN_OFF_TIME);
    }

    @Override
    public void right_hand_OK() {
        processOkTime(warning_right_hand_near_head, right_hand_OK, Configuration.GUARD_HAND_NEAR_HEAD_TURN_OFF_TIME);
    }

    /**
     * Nadpisuje wyniki analizy, które mają zostać zapisane w pliku .csv
     */
    public void updateDetectionResults(GuardParam guardParam) {
        this.guardParam = guardParam;
    }

    @Override
    public void checkWarnings() {
        times.forEach(time -> time.set(time.get() + STEP));

        alerts.put(Warning.HEAD_TILT_X, shouldRaiseAlert(warning_head_tilt_x, Configuration.WARNING_DURATION_HEAD_TILT));
        alerts.put(Warning.HEAD_TILT_Y, shouldRaiseAlert(warning_head_tilt_y, Configuration.WARNING_DURATION_HEAD_TILT));
        alerts.put(Warning.HEAD_TILT_Z, shouldRaiseAlert(warning_head_tilt_z, Configuration.WARNING_DURATION_HEAD_TILT));
        alerts.put(Warning.FACE_NOT_PRESENT, shouldRaiseAlert(warning_face_not_present, Configuration.WARNING_DURATION_FACE_NOT_PRESENT));
        alerts.put(Warning.EYES_CLOSED, shouldRaiseAlert(warning_eyes_closed, Configuration.WARNING_DURATION_EYES_CLOSED));
        alerts.put(Warning.LEFT_HAND_NEAR_HEAD,
                shouldRaiseAlert(warning_left_hand_near_head, Configuration.WARNING_DURATION_HAND_NEAR_HEAD));
        alerts.put(Warning.RIGHT_HAND_NEAR_HEAD,
                shouldRaiseAlert(warning_right_hand_near_head, Configuration.WARNING_DURATION_HAND_NEAR_HEAD));

        writeCsvRow();

        if (alerts.containsValue(true)) {
            //   SoundUtils.alert();
        }

        frameIndex += Configuration.RECORD_ANALYZING_FRAME_STEP;
        timeElapsedInMillis += STEP;
    }

    /**
     * Sprawdza czy ostrzeżenie przekroczyło próg wywołania alarmu.
     *
     * @param warningDurationInMillis Czas trwania ostrzeżenia.
     * @param alertThreshold          Ilość sekund, po których ma się włączyć alarm.
     * @return true - jeśli trzeba włączyć alarm
     */
    public boolean shouldRaiseAlert(AtomicInteger warningDurationInMillis, int alertThreshold) {
        return warningDurationInMillis.get() >= alertThreshold * 1000;
    }

    private void writeCsvRow() {
        String[] data = new String[18]; // columns defined in CsvFormatter

        data[0] = String.valueOf(frameIndex);
        data[1] = String.valueOf(timeElapsedInMillis);
        data[2] = String.valueOf((int) guardParam.getHeadXAngle());
        data[3] = String.valueOf((int) guardParam.getHeadYAngle());
        data[4] = String.valueOf((int) guardParam.getHeadZAngle());
        data[5] = String.valueOf(guardParam.getSmilingProbability());
        data[6] = String.valueOf(guardParam.getEyesOpenProbability());
        data[7] = guardParam.getLeftElbowAngle() == Integer.MAX_VALUE ? "1000" : String.valueOf(guardParam.getLeftElbowAngle());
        data[8] = guardParam.getRightElbowAngle() == Integer.MAX_VALUE ? "1000" : String.valueOf(guardParam.getRightElbowAngle());
        data[9] = String.valueOf(guardParam.getLeftHandNearHeadRatio());
        data[10] = String.valueOf(guardParam.getRightHandNearHeadRatio());

        data[11] = alerts.get(Warning.HEAD_TILT_X) ? "1" : "0";
        data[12] = alerts.get(Warning.HEAD_TILT_Y) ? "1" : "0";
        data[13] = alerts.get(Warning.HEAD_TILT_Z) ? "1" : "0";
        data[14] = alerts.get(Warning.EYES_CLOSED) ? "1" : "0";
        data[15] = alerts.get(Warning.LEFT_HAND_NEAR_HEAD) ? "1" : "0";
        data[16] = alerts.get(Warning.RIGHT_HAND_NEAR_HEAD) ? "1" : "0";
        data[17] = alerts.get(Warning.FACE_NOT_PRESENT) ? "1" : "0";

        csvFormatter.addDataRow(data);
    }
}
