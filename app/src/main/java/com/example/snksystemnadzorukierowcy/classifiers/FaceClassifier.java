package com.example.snksystemnadzorukierowcy.classifiers;

import com.example.snksystemnadzorukierowcy.Configuration;
import com.example.snksystemnadzorukierowcy.analyzers.CalibratedData;
import com.example.snksystemnadzorukierowcy.analyzers.guards.Guard;
import com.example.snksystemnadzorukierowcy.analyzers.guards.GuardParam;
import com.google.mlkit.vision.face.Face;

public class FaceClassifier implements Classifier {

    private static final float avg_x = CalibratedData.getInstance().getAvgHeadEulerAngleX();
    private static final float avg_y = CalibratedData.getInstance().getAvgHeadEulerAngleY();
    private static final float avg_z = CalibratedData.getInstance().getAvgHeadEulerAngleZ();

    public static void analyzeHeadRotation(Face face, Guard guard, GuardParam guardParam) {
        if (Math.abs(face.getHeadEulerAngleX() - avg_x) > Configuration.MAXIMUM_HEAD_TILT_X) {
            guard.warn_head_tilt_x();
        } else {
            guard.head_tilt_x_OK();
        }

        if (Math.abs(face.getHeadEulerAngleY() - avg_y) > Configuration.MAXIMUM_HEAD_TILT_Y) {
            guard.warn_head_tilt_y();
        } else {
            guard.head_tilt_y_OK();
        }

        if (Math.abs(face.getHeadEulerAngleZ() - avg_z) > Configuration.MAXIMUM_HEAD_TILT_Z) {
            guard.warn_head_tilt_z();
        } else {
            guard.head_tilt_z_OK();
        }

        guardParam.setHeadXAngle(Math.abs(face.getHeadEulerAngleX() - avg_x));
        guardParam.setHeadYAngle(Math.abs(face.getHeadEulerAngleY() - avg_y));
        guardParam.setHeadZAngle(Math.abs(face.getHeadEulerAngleZ() - avg_z));
    }

    public static void analyzeEyes(Face face, Guard guard, GuardParam guardParam) {
        if (face.getSmilingProbability() == null || face.getLeftEyeOpenProbability() == null || face.getRightEyeOpenProbability() == null) {
            return;
        }

        float eyesOpenProbability = (face.getLeftEyeOpenProbability() + face.getRightEyeOpenProbability()) / 2.0f;

        // jeśli twarz jest uśmiechnięta, oczy się przymykają, więc nie ma sensu ich analizować
        if (face.getSmilingProbability() >= Configuration.MINIMUM_SMILING_PROBABILITY) {
            guard.eyes_open_OK();
        } else {
            if (eyesOpenProbability >= Configuration.MINIMUM_EYES_OPEN_PROBABILITY) {
                guard.eyes_open_OK();
            } else {
                guard.warn_eyes_closed();
            }
        }

        guardParam.setSmilingProbability(face.getSmilingProbability());
        guardParam.setEyesOpenProbability(eyesOpenProbability);
    }
}
