package com.example.snksystemnadzorukierowcy.classifiers;

/**
 * Rezultat klasyfikacji pozy
 */
public class PoseClassificationResult {

    private int leftElbowAngle;
    private int rightElbowAngle;

    private float earToEarDistance;
    private float leftHandToFaceDistance;
    private float rightHandToFaceDistance;

    public int getLeftElbowAngle() {
        return leftElbowAngle;
    }

    public void setLeftElbowAngle(int leftElbowAngle) {
        this.leftElbowAngle = leftElbowAngle;
    }

    public int getRightElbowAngle() {
        return rightElbowAngle;
    }

    public void setRightElbowAngle(int rightElbowAngle) {
        this.rightElbowAngle = rightElbowAngle;
    }

    public float getEarToEarDistance() {
        return earToEarDistance;
    }

    public void setEarToEarDistance(float earToEarDistance) {
        this.earToEarDistance = earToEarDistance;
    }

    public float getLeftHandToFaceDistance() {
        return leftHandToFaceDistance;
    }

    public void setLeftHandToFaceDistance(float leftHandToFaceDistance) {
        this.leftHandToFaceDistance = leftHandToFaceDistance;
    }

    public float getRightHandToFaceDistance() {
        return rightHandToFaceDistance;
    }

    public void setRightHandToFaceDistance(float rightHandToFaceDistance) {
        this.rightHandToFaceDistance = rightHandToFaceDistance;
    }
}
