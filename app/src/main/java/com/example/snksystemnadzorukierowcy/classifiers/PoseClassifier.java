package com.example.snksystemnadzorukierowcy.classifiers;

import android.graphics.PointF;

import com.example.snksystemnadzorukierowcy.Configuration;
import com.example.snksystemnadzorukierowcy.analyzers.guards.Guard;
import com.example.snksystemnadzorukierowcy.analyzers.guards.GuardParam;
import com.google.mlkit.vision.pose.Pose;
import com.google.mlkit.vision.pose.PoseLandmark;

import static java.lang.Math.atan2;
import static java.lang.Math.sqrt;

/**
 * Klasa klasyfikująca pozę kierowcy.
 */
public class PoseClassifier implements Classifier {

    private final static float POSE_LANDMARK_IN_FRAME_LIKELIHOOD_THRESHOLD = 0.2f;

    private static Guard guard;

    public static void calculateLandmarksDistances(final PoseClassificationResult result, Pose pose) {

        final PoseLandmark leftHand = pose.getPoseLandmark(PoseLandmark.LEFT_PINKY);
        final PoseLandmark rightHand = pose.getPoseLandmark(PoseLandmark.RIGHT_PINKY);

        final PoseLandmark leftEar = pose.getPoseLandmark(PoseLandmark.LEFT_EAR);
        final PoseLandmark rightEar = pose.getPoseLandmark(PoseLandmark.RIGHT_EAR);

        final PoseLandmark nose = pose.getPoseLandmark(PoseLandmark.NOSE);

        final float earToEarDistance = calculateDistanceBetweenPoints(leftEar, rightEar);
        final float leftHandToFaceDistance = calculateDistanceBetweenPoints(leftHand, nose);
        final float rightHandToFaceDistance = calculateDistanceBetweenPoints(rightHand, nose);

        result.setEarToEarDistance(earToEarDistance);
        result.setLeftHandToFaceDistance(leftHandToFaceDistance);
        result.setRightHandToFaceDistance(rightHandToFaceDistance);
    }

    private static float calculateDistanceBetweenPoints(PoseLandmark start, PoseLandmark end) {
        if (start == null || end == null ||
                start.getInFrameLikelihood() <= POSE_LANDMARK_IN_FRAME_LIKELIHOOD_THRESHOLD ||
                end.getInFrameLikelihood() <= POSE_LANDMARK_IN_FRAME_LIKELIHOOD_THRESHOLD) {
            return Float.MAX_VALUE;
        }

        PointF startPoint = start.getPosition();
        PointF endPoint = end.getPosition();

        double x = Math.abs(startPoint.x - endPoint.x);
        double y = Math.abs(startPoint.y - endPoint.y);

        return (float) sqrt(x * x + y * y);
    }

    @SuppressWarnings("ConstantConditions")
    // - metoda jest wywoływana z SNKAnalyzer i wcześniej już sprawdzamy, czy pose != null
    public static void calculateArmAngles(final PoseClassificationResult result, Pose pose) {
        final double leftElbowAngle = getAngle(
                pose.getPoseLandmark(PoseLandmark.LEFT_SHOULDER),
                pose.getPoseLandmark(PoseLandmark.LEFT_ELBOW),
                pose.getPoseLandmark(PoseLandmark.LEFT_WRIST));
        result.setLeftElbowAngle((int) leftElbowAngle);

        final double rightElbowAngle = getAngle(
                pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER),
                pose.getPoseLandmark(PoseLandmark.RIGHT_ELBOW),
                pose.getPoseLandmark(PoseLandmark.RIGHT_WRIST));
        result.setRightElbowAngle((int) rightElbowAngle);
    }

    public static void classifyPose(final PoseClassificationResult result, GuardParam guardParam) {
        float leftHandRatio = 1000;
        float rightHandRatio = 1000;

        if (result.getLeftElbowAngle() <= Configuration.POSE_CLASSIFICATION_BENT_ARM_ANGLE_THRESHOLD) {
            leftHandRatio = result.getLeftHandToFaceDistance() / result.getEarToEarDistance();
            if (leftHandRatio <= Configuration.POSE_CLASSIFICATION_HAND_NEAR_HEAD_DETECTION_THRESHOLD) {
                guard.warn_left_hand_near_head_present();
            }
        } else {
            guard.left_hand_OK();
        }

        if (result.getRightElbowAngle() <= Configuration.POSE_CLASSIFICATION_BENT_ARM_ANGLE_THRESHOLD) {
            rightHandRatio = result.getRightHandToFaceDistance() / result.getEarToEarDistance();
            if (rightHandRatio <= Configuration.POSE_CLASSIFICATION_HAND_NEAR_HEAD_DETECTION_THRESHOLD) {
                guard.warn_right_hand_near_head_present();
            }
        } else {
            guard.right_hand_OK();
        }

        guardParam.setLeftElbowAngle(result.getLeftElbowAngle());
        guardParam.setRightElbowAngle(result.getRightElbowAngle());
        guardParam.setLeftHandNearHeadRatio(leftHandRatio);
        guardParam.setRightHandNearHeadRatio(rightHandRatio);
    }

    private static double getAngle(PoseLandmark firstPoint, PoseLandmark midPoint, PoseLandmark lastPoint) {
        if (
                firstPoint.getInFrameLikelihood() <= Configuration.POSE_LANDMARK_MINIMUM_BORDER_IN_FRAME_LIKELIHOOD ||
                        lastPoint.getInFrameLikelihood() <= Configuration.POSE_LANDMARK_MINIMUM_BORDER_IN_FRAME_LIKELIHOOD) {
            return Double.MAX_VALUE;
        }

        double result =
                Math.toDegrees(
                        atan2(lastPoint.getPosition().y - midPoint.getPosition().y,
                                lastPoint.getPosition().x - midPoint.getPosition().x)
                                - atan2(firstPoint.getPosition().y - midPoint.getPosition().y,
                                firstPoint.getPosition().x - midPoint.getPosition().x));
        result = Math.abs(result); // Angle should never be negative
        if (result > 180) {
            result = (360.0 - result); // Always get the acute representation of the angle
        }
        return result;
    }

    public static void setGuard(Guard guard) {
        PoseClassifier.guard = guard;
    }
}