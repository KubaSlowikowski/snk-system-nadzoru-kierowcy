package com.example.snksystemnadzorukierowcy.detectors;

import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;

/**
 * @param <R> Wynik detekcji
 */
public interface Detector<R> {

    Task<R> process(InputImage inputImage);
}
