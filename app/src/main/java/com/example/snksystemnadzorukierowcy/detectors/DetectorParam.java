package com.example.snksystemnadzorukierowcy.detectors;

import android.app.Activity;
import android.graphics.Bitmap;
import android.hardware.camera2.CameraCharacteristics;
import android.media.Image;
import android.util.Size;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.snksystemnadzorukierowcy.utils.WorkingMode;

/**
 * Klasa przechowujące wszystkie informacje, referencje potrzebne do poprawnej pracy detektora
 */
public class DetectorParam {

    private WorkingMode workingMode;

    private int rotation;
    /**
     * Rozdzielczość obrazu, który będzie przechwycony z kamery i dalej przetwarzany
     */
    private final Size imageSize;

    private final Activity activity;

    /**
     * Widok, na którym będą rysowane wykryte przez detektor obiekty
     */
    private final ImageView imageView;
    /**
     * Widok, na którym będą wyświetlane informacje dotyczące parametrów pracy detektora
     */
    private final TextView detectorDelayTextView;

    /**
     * Widok, na którym będą wyświetlane informacje dotyczące parametrów wykrytej twarzy,
     * np. kąty rotacji oraz prawdopodobieństwa zamkniętych oczu.
     */
    private final TextView faceParametersTextView;

    /**
     * Kamera, której aktualnie używamy (przednia, tylna, itd.).
     */
    private int cameraFacing = CameraCharacteristics.LENS_FACING_BACK;

    /**
     * Obraz prosto z kamery. Bez jakichkolwiek przekształceń.
     */
    private Image image;

    private Bitmap bitmap;

    private int frameRate;

    public DetectorParam(Size imageSize,
                         Activity activity,
                         ImageView imageView,
                         TextView faceParametersTextView,
                         WorkingMode workingMode) {
        this(imageSize, activity, imageView, null, faceParametersTextView, workingMode);
    }

    public DetectorParam(Size imageSize,
                         Activity activity,
                         ImageView imageView,
                         TextView detectorDelayTextView,
                         TextView faceParametersTextView,
                         WorkingMode workingMode) {
        this.imageSize = imageSize;
        this.activity = activity;
        this.imageView = imageView;
        this.detectorDelayTextView = detectorDelayTextView;
        this.faceParametersTextView = faceParametersTextView;
        this.workingMode = workingMode;
    }

    /**
     * Aktualizuje główny parametr detektora o nowe dane
     *
     * @param image    Obraz z kamery
     * @param rotation Obrót kamery
     */
    public void update(Image image, int rotation) {
        this.image = image;
        this.rotation = rotation;
    }

    /**
     * Aktualizuje główny parametr detektora o nowe dane
     *
     * @param bitmap   Bitmapa
     * @param rotation Obrót kamery
     */
    public void update(Bitmap bitmap, int rotation) {
        this.bitmap = bitmap;
        this.rotation = rotation;
    }

    public WorkingMode getWorkingMode() {
        return workingMode;
    }

    public int getImageWidth() {
        return this.imageSize.getWidth();
    }

    public int getImageHeight() {
        return this.imageSize.getHeight();
    }

    public Size getImageSize() {
        return imageSize;
    }

    public Image getImage() {
        return image;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public int getRotation() {
        return rotation;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getDetectorDelayTextView() {
        return detectorDelayTextView;
    }

    public TextView getFaceParametersTextView() {
        return faceParametersTextView;
    }

    public void setCameraFacing(int cameraFacing) {
        this.cameraFacing = cameraFacing;
    }

    public int getCameraFacing() {
        return cameraFacing;
    }

    public Activity getActivity() {
        return activity;
    }

    public int getFrameRate() {
        return frameRate;
    }

    public void setFrameRate(int frameRate) {
        this.frameRate = frameRate;
    }
}
