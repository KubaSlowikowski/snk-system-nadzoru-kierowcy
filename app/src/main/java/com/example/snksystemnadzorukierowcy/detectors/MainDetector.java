package com.example.snksystemnadzorukierowcy.detectors;

public interface MainDetector {
    void process(DetectorParam detectorParam);

    Boolean getShouldWait();
}
