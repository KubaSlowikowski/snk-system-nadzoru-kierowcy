package com.example.snksystemnadzorukierowcy.detectors;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.snksystemnadzorukierowcy.Configuration;
import com.example.snksystemnadzorukierowcy.analyzers.Analyzer;
import com.example.snksystemnadzorukierowcy.analyzers.SNKAnalyzerParam;
import com.example.snksystemnadzorukierowcy.analyzers.SNKCalibrationAnalyzer;
import com.example.snksystemnadzorukierowcy.utils.WorkingMode;
import com.example.snksystemnadzorukierowcy.utils.drawers.FaceDrawer;
import com.example.snksystemnadzorukierowcy.utils.drawers.PoseDrawer;
import com.example.snksystemnadzorukierowcy.utils.image.ImageConverter;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.label.ImageLabel;
import com.google.mlkit.vision.pose.Pose;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Główny detektor, który łączy działanie pozostałych detektorów
 */
public class SNKDetector implements MainDetector {

    /**
     * Instancja klasy. (wzorzec projektowy Singleton)
     */
    private static SNKDetector instance;

    /**
     * Detektor twarzy.
     */
    private final SNKFaceDetector snkFaceDetector = SNKFaceDetector.getInstance();
    /**
     * Detektor pozy.
     */
    private final SNKPoseDetector snkPoseDetector = SNKPoseDetector.getInstance();

    /**
     * Analizator danych z detektorów.
     */
    private Analyzer analyzer = SNKCalibrationAnalyzer.getInstance();
    private SNKAnalyzerParam analyzerParam;

    /**
     * Zmienna logiczna mówiąca, czy obraz otrzymany z kamery powinien być
     * przekazany do detektora. Jeśli <i>false</i>, to poprzedni obraz jest wciąż przetwarzany
     * przez detektor i aktualny ma zostać pominięty
     */
    private Boolean shouldWait = false;

    private Instant start;

    /**
     * 'Farba', którą będziemy rysować puknty charakterystyczne twarzy
     */
    private final Paint facePaint;

    /**
     * Zmienna logiczna mówiąca o tym, czy w danej iteracji detektor pozy powiniem działać.
     */
    private boolean shouldDetectPose = false;

    /**
     * Wykryta poza kierowcy.
     */
    private Pose pose;

    /**
     * Zlicza klatki. Pomaga w dobraniu odpowiedniej częstotliwości załączeń detektora pozy.
     */
    private int poseFramesCounter = 0;

    /**
     * Raz utworzona bitmapa, dzięki której nie musimy tworzyć od nowa bitmap na każdą klatkę.
     */
    private Bitmap constBitmap;

    private SNKDetector() {
        facePaint = new Paint();
        facePaint.setStyle(Paint.Style.STROKE);
        facePaint.setColor(Color.RED);
        facePaint.setStrokeWidth(1.5f);
    }

    public static SNKDetector getInstance() {
        if (instance == null) {
            instance = new SNKDetector();
        }
        return instance;
    }

    @Override
    public void process(DetectorParam param) {
        shouldWait = true;

        start = Instant.now();

        Bitmap bitmap;
        InputImage inputImage;

        if (param.getWorkingMode() == WorkingMode.LIVE) {
            if (constBitmap == null) {
                constBitmap = Bitmap.createBitmap(param.getImageHeight(), param.getImageWidth(), Bitmap.Config.ARGB_8888);
            }
            bitmap = constBitmap.copy(Bitmap.Config.ARGB_8888, true);
            inputImage = ImageConverter.fromImageToInputImage(param.getImage(), param.getRotation());
        } else {
            bitmap = param.getBitmap();
            inputImage = InputImage.fromBitmap(bitmap, param.getRotation());
        }

        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(bitmap, 0, 0, facePaint);

        determineIfShouldDetectPose();

        final Task<List<Face>> faceDetectorResult = snkFaceDetector.process(inputImage);
        final AtomicReference<Task<Pose>> atomicPoseResultReference = new AtomicReference<>(); // https://stackoverflow.com/questions/34865383/variable-used-in-lambda-expression-should-be-final-or-effectively-final

        if (shouldDetectPose) {
            atomicPoseResultReference.set(snkPoseDetector.process(inputImage));
        }

        analyzerParam = new SNKAnalyzerParam(param.getActivity());
        analyzerParam.setWorkingMode(param.getWorkingMode());

        if (param.getWorkingMode() == WorkingMode.RECORD) {
            analyzerParam.setFrameRate(param.getFrameRate());
        }

        faceDetectorResult
                .addOnSuccessListener(faces -> {
                    if (faces.size() == 0) {
                        param.getFaceParametersTextView().setText("");
                        cleanUp(param, bitmap);
                    } else {
                        processFace(faces.stream().findFirst().get(), param, canvas);
                        processPose(param, canvas, bitmap, atomicPoseResultReference.get());
                    }
                });
    }

    private void determineIfShouldDetectPose() {
        poseFramesCounter++;
        if (poseFramesCounter == Configuration.POSE_DETECTION_FREQUENCY) {
            poseFramesCounter = 0;
            shouldDetectPose = true;
        } else {
            shouldDetectPose = false;
        }
    }

    private void countDetectorDelay(DetectorParam param) {
        Instant now = Instant.now();
        Duration timeElapsed = Duration.between(start, now);
        if (param.getDetectorDelayTextView() != null && timeElapsed.toMillis() > 0) {
            param.getDetectorDelayTextView().setText("Detector delay: " + timeElapsed.toMillis() + " ms");
        }
    }

    private void cleanUp(DetectorParam param, Bitmap bitmap) {
        param.getActivity().runOnUiThread(() -> param.getImageView().setImageBitmap(bitmap));
        if (param.getImage() != null) {
            param.getImage().close();
        }

        analyzer.analyze(analyzerParam);

        countDetectorDelay(param);
        shouldWait = false;
    }

    /**
     * Wstępne przetworzenie pozy, tj:<br>
     * - narysowanie jej na ekranie <br>
     * - przypisanie jej do zmiennej, która będzie użyte w kolejnych iteracjach (poza jest wykrywana co X klatek) <br>
     * - przypisanie jej do parametru analizatora
     */
    private void processPose(DetectorParam param, Canvas canvas, Bitmap bitmap, Task<Pose> poseDetectorResult) {
        if (shouldDetectPose) {
            poseDetectorResult
                    .addOnSuccessListener(pose -> {
                        if (pose.getAllPoseLandmarks().size() == 0) {
                            return;
                        }

                        this.pose = pose;
                        analyzerParam.setPose(pose);

                        PoseDrawer.drawOnlyHandsAndShoulders(pose, canvas, param.getCameraFacing(), param.getImageSize());
                    })
                    .addOnCompleteListener(pose -> cleanUp(param, bitmap));
        } else {
            if (pose != null) {
                PoseDrawer.drawOnlyHandsAndShoulders(pose, canvas, param.getCameraFacing(), param.getImageSize());
            }
            cleanUp(param, bitmap);
        }
    }

    /**
     * Wstępne przetworzenie twarzy, tj:<br>
     * - narysowanie jej na ekranie <br>
     * - przypisanie jej do parametru analizatora
     */
    private void processFace(Face face, DetectorParam param, Canvas canvas) {
        analyzerParam.setFace(face);
        FaceDrawer.drawFace(face, param, canvas, facePaint);
    }

    @Override
    public Boolean getShouldWait() {
        return shouldWait;
    }

    public void setAnalyzer(Analyzer analyzer) {
        this.analyzer = analyzer;
    }
}