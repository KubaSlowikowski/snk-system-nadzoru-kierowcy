package com.example.snksystemnadzorukierowcy.detectors;

import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;

import java.util.List;

public class SNKFaceDetector implements Detector<List<Face>> {

    private final FaceDetector faceDetector;
    private static SNKFaceDetector instance;

    private SNKFaceDetector() {
        FaceDetectorOptions faceDetectorOptions = new FaceDetectorOptions.Builder()
                .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
//                .setContourMode(FaceDetectorOptions.CONTOUR_MODE_NONE)
                .setContourMode(FaceDetectorOptions.CONTOUR_MODE_ALL)
                .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                .build();
        faceDetector = FaceDetection.getClient(faceDetectorOptions);
    }

    public static SNKFaceDetector getInstance() {
        if (instance == null) {
            instance = new SNKFaceDetector();
        }
        return instance;
    }

    @Override
    public Task<List<Face>> process(InputImage inputImage) {
        return faceDetector.process(inputImage);
    }
}