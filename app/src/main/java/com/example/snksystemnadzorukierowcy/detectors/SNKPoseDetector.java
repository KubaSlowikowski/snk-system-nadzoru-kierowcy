package com.example.snksystemnadzorukierowcy.detectors;

import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.pose.Pose;
import com.google.mlkit.vision.pose.PoseDetection;
import com.google.mlkit.vision.pose.PoseDetector;
import com.google.mlkit.vision.pose.defaults.PoseDetectorOptions;

class SNKPoseDetector implements Detector<Pose> {

    private final PoseDetector poseDetector;
    private static SNKPoseDetector instance;

    private SNKPoseDetector() {
        PoseDetectorOptions poseDetectorOptions = new PoseDetectorOptions.Builder()
                .setDetectorMode(PoseDetectorOptions.STREAM_MODE)
                .build();
        poseDetector = PoseDetection.getClient(poseDetectorOptions);
    }

    public static SNKPoseDetector getInstance() {
        if (instance == null) {
            instance = new SNKPoseDetector();
        }
        return instance;
    }

    @Override
    public Task<Pose> process(InputImage inputImage) {
        return poseDetector.process(inputImage);
    }
}