package com.example.snksystemnadzorukierowcy.utils;

import android.util.Size;

import java.util.Comparator;

/**
 * Klasa służąca do pomocy w dobraniu odpowiedniej rozdzielczości kamery, aby dobrze wyświetlić podgląd
 */
public class CompareSizeByArea implements Comparator<Size> {
    @Override
    public int compare(Size o1, Size o2) { // sortuje rosnąco
        return Long.compare(o1.getWidth() * o1.getHeight(), o2.getWidth() * o2.getHeight());
    }
}