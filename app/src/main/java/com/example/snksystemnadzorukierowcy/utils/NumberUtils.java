package com.example.snksystemnadzorukierowcy.utils;

import java.math.BigDecimal;

public class NumberUtils {
    public static float round(float number) {
        return round(number, 1);
    }

    public static float round(float number, int scale) {
        return BigDecimal.valueOf(number).setScale(scale, BigDecimal.ROUND_HALF_DOWN).floatValue();
    }

    public static double round(double number, int scale) {
        return BigDecimal.valueOf(number).setScale(scale, BigDecimal.ROUND_HALF_DOWN).doubleValue();
    }
}
