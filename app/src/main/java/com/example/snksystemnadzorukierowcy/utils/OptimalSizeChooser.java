package com.example.snksystemnadzorukierowcy.utils;

import android.util.Size;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class OptimalSizeChooser {

    public static Size chooseOptimalSize(Size[] availableCameraResolutions, final int width, final int height) { // szuka najbardziej odpowiedniej rozdzielczosci
        final Size minImageToProcessSize = new Size(480, 360); // minimalna rozdzielczosc zdjecia umozliwiajaca podpiecie sieci ML Kit
        double screenSizeRatio = (double) height / width;
        screenSizeRatio = Math.round(screenSizeRatio * 10.0) / 10.0; // zaokrąglenie do jednego miejsca po przecinku
        List<Size> goodResolutions = new ArrayList<>(); //czy rozdzielczosc z sensora (kamery) jest wystarczajaco duza na nasz wyswietlacz
        for (Size resolution : availableCameraResolutions) { // sprawdzamy czy propocje wielkosci obrazu z kamery odpowiadaja tym z TextureView oraz czy rozdzielczosc z sensora (kamery) jest wystarczajaco duza na nasz wyswietlacz
            double ratio = (double) resolution.getWidth() / resolution.getHeight();
            ratio = Math.round(ratio * 10.0) / 10.0;
            if (ratio == screenSizeRatio) {
                goodResolutions.add(resolution);
            }
        }
        if (goodResolutions.size() > 0) {
            goodResolutions.sort(new CompareSizeByArea());
            goodResolutions = goodResolutions.stream().filter(resolution -> (
                    resolution.getWidth() >= minImageToProcessSize.getWidth() &&
                            resolution.getHeight() >= minImageToProcessSize.getHeight())).collect(Collectors.toList());
            return Collections.min(goodResolutions, new CompareSizeByArea()); // bierzemy najmniejsza pasujaca rozdzielczosc
        } else {
            return availableCameraResolutions[0];
        }
    }
}
