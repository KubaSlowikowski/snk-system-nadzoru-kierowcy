package com.example.snksystemnadzorukierowcy.utils;

/**
 * Klasa potrzebna do analizy nagrania.
 * Zawiera potrzebne metadane.
 */
public class RecordParam {

    private int durationInMillis;
    private int framesCount;
    private int currentPositionInMillis;
    private int currentFrameIndex;

    private int frameRate; // FPS

    private int height;
    private int width;

    public RecordParam() {
    }

    public int getDurationInMillis() {
        return durationInMillis;
    }

    public void setDurationInMillis(int durationInMillis) {
        this.durationInMillis = durationInMillis;
    }

    public int getFramesCount() {
        return framesCount;
    }

    public void setFramesCount(int framesCount) {
        this.framesCount = framesCount;
    }

    public int getCurrentPositionInMillis() {
        return currentPositionInMillis;
    }

    public void setCurrentPositionInMillis(int currentPositionInMillis) {
        this.currentPositionInMillis = currentPositionInMillis;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getCurrentFrameIndex() {
        return currentFrameIndex;
    }

    public void setCurrentFrameIndex(int currentFrameIndex) {
        this.currentFrameIndex = currentFrameIndex;
    }

    public int getFrameRate() {
        return frameRate;
    }

    public void setFrameRate(int frameRate) {
        this.frameRate = frameRate;
    }
}
