package com.example.snksystemnadzorukierowcy.utils;

import android.media.AudioManager;
import android.media.ToneGenerator;

public abstract class SoundUtils {
    private static final ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);

    public static void beep() {
        toneGenerator.startTone(ToneGenerator.TONE_CDMA_PIP, 150);
    }

    public static void doubleBeep() {
        toneGenerator.startTone(ToneGenerator.TONE_CDMA_PIP, 400);
    }

    public static void alert() {
        toneGenerator.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 250);

    }

    public static void hardAlert() {
        toneGenerator.startTone(ToneGenerator.TONE_CDMA_HIGH_PBX_S_X4, 250);
    }
}
