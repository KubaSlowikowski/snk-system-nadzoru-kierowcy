package com.example.snksystemnadzorukierowcy.utils;

public enum WorkingMode {
    RECORD,
    LIVE
}
