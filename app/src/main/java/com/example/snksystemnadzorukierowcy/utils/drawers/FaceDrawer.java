package com.example.snksystemnadzorukierowcy.utils.drawers;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.camera2.CameraCharacteristics;

import com.example.snksystemnadzorukierowcy.detectors.DetectorParam;
import com.example.snksystemnadzorukierowcy.utils.NumberUtils;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceContour;

import java.util.List;

/**
 * Klasa służąca do rysowania konturów twarzy
 */
public class FaceDrawer {

    /**
     * Rysuje twarz na podanym 'canvas'
     *
     * @param face      Wykryta twarz
     * @param param     Parametr detektora
     * @param canvas    Warstwa, na której 'rysujemy'
     * @param facePaint 'Farba', którą rysujemy
     */
    public static void drawFace(Face face, DetectorParam param, Canvas canvas, Paint facePaint) {
        List<FaceContour> faceContours = face.getAllContours();
        faceContours.forEach(faceContour -> {
            faceContour.getPoints().forEach(pointF -> {
                if (param.getCameraFacing() == CameraCharacteristics.LENS_FACING_BACK) {
                    canvas.drawCircle(param.getImageHeight() - pointF.x, pointF.y, 2, facePaint);
                } else {
                    canvas.drawCircle(pointF.x, pointF.y, 2, facePaint);
                }
            });
        });
    }

    /**
     * Zamieszcza na UI informacje m.in. kątach  rotacji twarzy,
     * prawdopodobieństwie otwarcia oczu.
     *
     * @param face  Wykryta twarz
     * @param param Parametr detektora
     */
    public static void drawFaceParameters(Face face, DetectorParam param) {
        StringBuilder stringBuilder = new StringBuilder();

        addEulerAngles(stringBuilder, face);
        addProbabilities(stringBuilder, face);

        param.getFaceParametersTextView().setText(stringBuilder.toString());
    }

    private static void addEulerAngles(StringBuilder stringBuilder, Face face) {
        float x = face.getHeadEulerAngleX();
        float y = face.getHeadEulerAngleY();
        float z = face.getHeadEulerAngleZ();
        stringBuilder
                .append("X: ").append(NumberUtils.round(x)).append(" [deg]\n")
                .append("Y: ").append(NumberUtils.round(y)).append(" [deg]\n")
                .append("Z: ").append(NumberUtils.round(z)).append(" [deg]\n");
    }

    private static void addProbabilities(StringBuilder stringBuilder, Face face) {
        int rightEyeOpenProb = -1;
        int leftEyeOpenProb = -1;
        if (face.getRightEyeOpenProbability() != null && face.getLeftEyeOpenProbability() != null) {
            rightEyeOpenProb = (int) (face.getRightEyeOpenProbability() * 100);
            leftEyeOpenProb = (int) (face.getLeftEyeOpenProbability() * 100);
        }

        stringBuilder
                .append("Right eye open: ").append(rightEyeOpenProb).append("\n")
                .append("Left eye open: ").append(leftEyeOpenProb).append("\n");
    }
}
