package com.example.snksystemnadzorukierowcy.utils.drawers;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.hardware.camera2.CameraCharacteristics;
import android.util.Size;

import com.example.snksystemnadzorukierowcy.Configuration;
import com.google.mlkit.vision.pose.Pose;
import com.google.mlkit.vision.pose.PoseLandmark;

import java.util.List;

public class PoseDrawer {

    private static final float DOT_RADIUS = 5.0f;
    private static final float IN_FRAME_LIKELIHOOD_TEXT_SIZE = 15.0f;
    private static final float STROKE_WIDTH = 5.0f;

    private static final Paint leftPaint;
    private static final Paint rightPaint;
    private static final Paint whitePaint;

    private static final List<Integer> faceLandmarks = List.of(
            PoseLandmark.NOSE,
            PoseLandmark.LEFT_EYE_INNER,
            PoseLandmark.LEFT_EYE,
            PoseLandmark.LEFT_EYE_OUTER,
            PoseLandmark.RIGHT_EYE_INNER,
            PoseLandmark.RIGHT_EYE,
            PoseLandmark.RIGHT_EYE_OUTER,
            PoseLandmark.LEFT_EAR,
            PoseLandmark.RIGHT_EAR,
            PoseLandmark.LEFT_MOUTH,
            PoseLandmark.RIGHT_MOUTH
    );

    private static final List<Integer> lowerBodyAndFaceLandmarks = List.of(
            // FACE
            PoseLandmark.NOSE,
            PoseLandmark.LEFT_EYE_INNER,
            PoseLandmark.LEFT_EYE,
            PoseLandmark.LEFT_EYE_OUTER,
            PoseLandmark.RIGHT_EYE_INNER,
            PoseLandmark.RIGHT_EYE,
            PoseLandmark.RIGHT_EYE_OUTER,
            PoseLandmark.LEFT_EAR,
            PoseLandmark.RIGHT_EAR,
            PoseLandmark.LEFT_MOUTH,
            PoseLandmark.RIGHT_MOUTH,

            // LOWER BODY
            PoseLandmark.LEFT_HIP,
            PoseLandmark.RIGHT_HIP,
            PoseLandmark.LEFT_KNEE,
            PoseLandmark.RIGHT_KNEE,
            PoseLandmark.LEFT_ANKLE,
            PoseLandmark.RIGHT_ANKLE,
            PoseLandmark.LEFT_HEEL,
            PoseLandmark.RIGHT_HEEL,
            PoseLandmark.LEFT_FOOT_INDEX,
            PoseLandmark.RIGHT_FOOT_INDEX
    );

    static {
        whitePaint = new Paint();
        whitePaint.setStrokeWidth(STROKE_WIDTH);
        whitePaint.setColor(Color.WHITE);
        whitePaint.setTextSize(IN_FRAME_LIKELIHOOD_TEXT_SIZE);
        leftPaint = new Paint();
        leftPaint.setStrokeWidth(STROKE_WIDTH);
        leftPaint.setColor(Color.GREEN);
        rightPaint = new Paint();
        rightPaint.setStrokeWidth(STROKE_WIDTH);
        rightPaint.setColor(Color.YELLOW);
    }

    public static void drawOnlyHandsAndShoulders(Pose pose, Canvas canvas, int cameraFacing, Size imageSize) {
        List<PoseLandmark> landmarks = pose.getAllPoseLandmarks();
        boolean flip = cameraFacing == CameraCharacteristics.LENS_FACING_BACK;

        // Draw all the points
        for (PoseLandmark landmark : landmarks) {
            if (!isLandmarkOnFaceOrLowerBody(landmark)) {
                drawPoint(canvas, landmark, whitePaint, flip, imageSize);
            }
        }

        PoseLandmark leftShoulder = pose.getPoseLandmark(PoseLandmark.LEFT_SHOULDER);
        PoseLandmark rightShoulder = pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER);
        PoseLandmark leftElbow = pose.getPoseLandmark(PoseLandmark.LEFT_ELBOW);
        PoseLandmark rightElbow = pose.getPoseLandmark(PoseLandmark.RIGHT_ELBOW);
        PoseLandmark leftWrist = pose.getPoseLandmark(PoseLandmark.LEFT_WRIST);
        PoseLandmark rightWrist = pose.getPoseLandmark(PoseLandmark.RIGHT_WRIST);

        PoseLandmark leftIndex = pose.getPoseLandmark(PoseLandmark.LEFT_INDEX);
        PoseLandmark rightIndex = pose.getPoseLandmark(PoseLandmark.RIGHT_INDEX);
        PoseLandmark leftThumb = pose.getPoseLandmark(PoseLandmark.LEFT_THUMB);
        PoseLandmark rightThumb = pose.getPoseLandmark(PoseLandmark.RIGHT_THUMB);
        PoseLandmark leftPinky = pose.getPoseLandmark(PoseLandmark.LEFT_PINKY);
        PoseLandmark rightPinky = pose.getPoseLandmark(PoseLandmark.RIGHT_PINKY);

        drawLine(canvas, leftShoulder, rightShoulder, whitePaint, flip, imageSize);

        // Left body
        drawLine(canvas, leftShoulder, leftElbow, leftPaint, flip, imageSize);
        drawLine(canvas, leftElbow, leftWrist, leftPaint, flip, imageSize);
        drawLine(canvas, leftWrist, leftThumb, leftPaint, flip, imageSize);
        drawLine(canvas, leftWrist, leftPinky, leftPaint, flip, imageSize);
        drawLine(canvas, leftWrist, leftIndex, leftPaint, flip, imageSize);
        drawLine(canvas, leftIndex, leftPinky, leftPaint, flip, imageSize);

        // Right body
        drawLine(canvas, rightShoulder, rightElbow, rightPaint, flip, imageSize);
        drawLine(canvas, rightElbow, rightWrist, rightPaint, flip, imageSize);
        drawLine(canvas, rightWrist, rightThumb, rightPaint, flip, imageSize);
        drawLine(canvas, rightWrist, rightPinky, rightPaint, flip, imageSize);
        drawLine(canvas, rightWrist, rightIndex, rightPaint, flip, imageSize);
        drawLine(canvas, rightIndex, rightPinky, rightPaint, flip, imageSize);
    }

    public static void drawPose(Pose pose, Canvas canvas, int cameraFacing, Size imageSize) {
        List<PoseLandmark> landmarks = pose.getAllPoseLandmarks();
        boolean flip = cameraFacing == CameraCharacteristics.LENS_FACING_BACK;

        // Draw all the points
        for (PoseLandmark landmark : landmarks) {
            if (!isLandmarkOnFace(landmark)) {
                drawPoint(canvas, landmark, whitePaint, flip, imageSize);
            }
        }

        PoseLandmark leftShoulder = pose.getPoseLandmark(PoseLandmark.LEFT_SHOULDER);
        PoseLandmark rightShoulder = pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER);
        PoseLandmark leftElbow = pose.getPoseLandmark(PoseLandmark.LEFT_ELBOW);
        PoseLandmark rightElbow = pose.getPoseLandmark(PoseLandmark.RIGHT_ELBOW);
        PoseLandmark leftWrist = pose.getPoseLandmark(PoseLandmark.LEFT_WRIST);
        PoseLandmark rightWrist = pose.getPoseLandmark(PoseLandmark.RIGHT_WRIST);
        PoseLandmark leftHip = pose.getPoseLandmark(PoseLandmark.LEFT_HIP);
        PoseLandmark rightHip = pose.getPoseLandmark(PoseLandmark.RIGHT_HIP);
        PoseLandmark leftKnee = pose.getPoseLandmark(PoseLandmark.LEFT_KNEE);
        PoseLandmark rightKnee = pose.getPoseLandmark(PoseLandmark.RIGHT_KNEE);
        PoseLandmark leftAnkle = pose.getPoseLandmark(PoseLandmark.LEFT_ANKLE);
        PoseLandmark rightAnkle = pose.getPoseLandmark(PoseLandmark.RIGHT_ANKLE);

        PoseLandmark leftPinky = pose.getPoseLandmark(PoseLandmark.LEFT_PINKY);
        PoseLandmark rightPinky = pose.getPoseLandmark(PoseLandmark.RIGHT_PINKY);
        PoseLandmark leftIndex = pose.getPoseLandmark(PoseLandmark.LEFT_INDEX);
        PoseLandmark rightIndex = pose.getPoseLandmark(PoseLandmark.RIGHT_INDEX);
        PoseLandmark leftThumb = pose.getPoseLandmark(PoseLandmark.LEFT_THUMB);
        PoseLandmark rightThumb = pose.getPoseLandmark(PoseLandmark.RIGHT_THUMB);
        PoseLandmark leftHeel = pose.getPoseLandmark(PoseLandmark.LEFT_HEEL);
        PoseLandmark rightHeel = pose.getPoseLandmark(PoseLandmark.RIGHT_HEEL);
        PoseLandmark leftFootIndex = pose.getPoseLandmark(PoseLandmark.LEFT_FOOT_INDEX);
        PoseLandmark rightFootIndex = pose.getPoseLandmark(PoseLandmark.RIGHT_FOOT_INDEX);

        drawLine(canvas, leftShoulder, rightShoulder, whitePaint, flip, imageSize);
        drawLine(canvas, leftHip, rightHip, whitePaint, flip, imageSize);

        // Left body
        drawLine(canvas, leftShoulder, leftElbow, leftPaint, flip, imageSize);
        drawLine(canvas, leftElbow, leftWrist, leftPaint, flip, imageSize);
        drawLine(canvas, leftShoulder, leftHip, leftPaint, flip, imageSize);
        drawLine(canvas, leftHip, leftKnee, leftPaint, flip, imageSize);
        drawLine(canvas, leftKnee, leftAnkle, leftPaint, flip, imageSize);
        drawLine(canvas, leftWrist, leftThumb, leftPaint, flip, imageSize);
        drawLine(canvas, leftWrist, leftPinky, leftPaint, flip, imageSize);
        drawLine(canvas, leftWrist, leftIndex, leftPaint, flip, imageSize);
        drawLine(canvas, leftIndex, leftPinky, leftPaint, flip, imageSize);
        drawLine(canvas, leftAnkle, leftHeel, leftPaint, flip, imageSize);
        drawLine(canvas, leftHeel, leftFootIndex, leftPaint, flip, imageSize);

        // Right body
        drawLine(canvas, rightShoulder, rightElbow, rightPaint, flip, imageSize);
        drawLine(canvas, rightElbow, rightWrist, rightPaint, flip, imageSize);
        drawLine(canvas, rightShoulder, rightHip, rightPaint, flip, imageSize);
        drawLine(canvas, rightHip, rightKnee, rightPaint, flip, imageSize);
        drawLine(canvas, rightKnee, rightAnkle, rightPaint, flip, imageSize);
        drawLine(canvas, rightWrist, rightThumb, rightPaint, flip, imageSize);
        drawLine(canvas, rightWrist, rightPinky, rightPaint, flip, imageSize);
        drawLine(canvas, rightWrist, rightIndex, rightPaint, flip, imageSize);
        drawLine(canvas, rightIndex, rightPinky, rightPaint, flip, imageSize);
        drawLine(canvas, rightAnkle, rightHeel, rightPaint, flip, imageSize);
        drawLine(canvas, rightHeel, rightFootIndex, rightPaint, flip, imageSize);
    }

    private static void drawPoint(Canvas canvas, PoseLandmark landmark, Paint paint, boolean flip, Size imageSize) {
        if (landmark.getInFrameLikelihood() <= Configuration.POSE_LANDMARK_MINIMUM_BORDER_IN_FRAME_LIKELIHOOD) {
            return;
        }
        PointF point = landmark.getPosition();
        if (flip) {
            canvas.drawText(String.valueOf(landmark.getInFrameLikelihood()), imageSize.getHeight() - point.x, point.y - 20, whitePaint);
            canvas.drawCircle(imageSize.getHeight() - point.x, point.y, DOT_RADIUS, paint);
        } else {
            canvas.drawText(String.valueOf(landmark.getInFrameLikelihood()), point.x, point.y - 20, whitePaint);
            canvas.drawCircle(point.x, point.y, DOT_RADIUS, paint);
        }
    }

    private static void drawLine(Canvas canvas, PoseLandmark startLandmark, PoseLandmark endLandmark, Paint paint, boolean flip, Size imageSize) {
        if (startLandmark == null || endLandmark == null) {
            return;
        }
        if (startLandmark.getInFrameLikelihood() <= Configuration.POSE_LANDMARK_MINIMUM_BORDER_IN_FRAME_LIKELIHOOD &&
                endLandmark.getInFrameLikelihood() <= Configuration.POSE_LANDMARK_MINIMUM_BORDER_IN_FRAME_LIKELIHOOD) {
            return;
        }
        PointF start = startLandmark.getPosition();
        PointF end = endLandmark.getPosition();
        if (flip) {
            canvas.drawLine(imageSize.getHeight() - start.x, start.y, imageSize.getHeight() - end.x, end.y, paint);
        } else {
            canvas.drawLine(start.x, start.y, end.x, end.y, paint);
        }
    }

    private static boolean isLandmarkOnFace(PoseLandmark poseLandmark) {
        return faceLandmarks.contains(poseLandmark.getLandmarkType());
    }

    private static boolean isLandmarkOnFaceOrLowerBody(PoseLandmark poseLandmark) {
        return lowerBodyAndFaceLandmarks.contains(poseLandmark.getLandmarkType());
    }
}
