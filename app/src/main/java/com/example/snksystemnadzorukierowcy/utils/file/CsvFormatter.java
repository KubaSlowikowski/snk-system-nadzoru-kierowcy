package com.example.snksystemnadzorukierowcy.utils.file;

import com.example.snksystemnadzorukierowcy.analyzers.CalibratedData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class CsvFormatter {

    public static CsvFormatter instance;

    private final List<String[]> dataLines;
    private final FileService fileService;

    private CsvFormatter() {
        fileService = new FileService();
        fileService.createCustomFolder();

        dataLines = new ArrayList<>();
        dataLines.add(new String[]{
                "Frame index",                  // 0
                "Time [ms]",                    // 1
                "Head X angle [deg]",           // 2
                "Head Y angle [deg]",           // 3
                "Head Z angle [deg]",           // 4
                "Smiling probability",          // 5
                "Eyes open probability",        // 6
                "Left elbow angle [deg]",       // 7
                "Right elbow angle [deg]",      // 8
                "Left hand to head ratio",      // 9
                "Right hand to head ratio",     // 10
                "Alert head_X",                 // 11
                "Alert head_Y",                 // 12
                "Alert head_Z",                 // 13
                "Alert eyes_closed",            // 14
                "Alert left_hand_near_head",    // 15
                "Alert right_hand_near_head",   // 16
                "Alert face_not_present",       // 17
        });
    }

    public static CsvFormatter getInstance() {
        if (instance == null) {
            instance = new CsvFormatter();
        }
        return instance;
    }

    public void addDataRow(String[] data) {
        dataLines.add(data);
    }

    public void createCsvFile(String fileName) {
        File csvOutputFile = new File(fileService.createFileName(fileName, FileFormat.CSV));
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String convertToCSV(String[] data) {
        return String.join(",", data);
    }

    public void createCsvCalibrationResultFile(String fileName, int frameIndex, int timeInMillis) {
        File csvOutputFile = new File(fileService.createFileName(fileName, FileFormat.CSV));
        String[] columns = new String[]{
                "Calibrated head X angle",
                "Calibrated head Y angle",
                "Calibrated head Z angle",
                "Frame index",
                "Time [ms]"
        };
        String[] data = new String[]{
                String.valueOf(CalibratedData.getInstance().getAvgHeadEulerAngleX()),
                String.valueOf(CalibratedData.getInstance().getAvgHeadEulerAngleY()),
                String.valueOf(CalibratedData.getInstance().getAvgHeadEulerAngleZ()),
                String.valueOf(frameIndex),
                String.valueOf(timeInMillis)
        };
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            pw.println(convertToCSV(columns));
            pw.println(convertToCSV(data));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}