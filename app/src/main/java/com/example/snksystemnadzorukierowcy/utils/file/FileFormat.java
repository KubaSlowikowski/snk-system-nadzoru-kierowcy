package com.example.snksystemnadzorukierowcy.utils.file;

public enum FileFormat {
    MP4(".mp4"),
    JPEG(".jpg"),
    CSV(".csv");

    private final String extension;

    FileFormat(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }
}
