package com.example.snksystemnadzorukierowcy.utils.file;

import android.os.Environment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileService {
    private File customFolder;

    public void createCustomFolder() {
        File movieFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        customFolder = new File(movieFile, "SNK");
        if (!customFolder.exists()) {
            customFolder.mkdirs(); // jeśli folder nie istnieje, tworzymy go
        }
    }

    public String createFileName(FileFormat fileFormat) { // tworzy unikalne nazwy filmów, które będziemy zapisywać
        String timestamp = new SimpleDateFormat("yyyyMMdd_HH:mm:ss").format(new Date());
        String prepend = "SNK_" + timestamp;
        return customFolder.getAbsolutePath() + "/" + prepend + fileFormat.getExtension();
    }

    public String createFileName(String name, FileFormat fileFormat) {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HH:mm:ss").format(new Date());
        return customFolder.getAbsolutePath() + "/" + name + "_" + timestamp + fileFormat.getExtension();
    }
}