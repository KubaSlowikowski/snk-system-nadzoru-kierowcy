package com.example.snksystemnadzorukierowcy.utils.image;

import android.graphics.Bitmap;
import android.media.Image;

import com.google.mlkit.vision.common.InputImage;

import java.nio.ByteBuffer;

public class ImageConverter {

    public static InputImage fromImageToInputImage2(Image image, int rotation) { // Szybsza metoda, ale nie działa tylko dla FaceDetectora. Używa tylko buffera Y z formatu YUV
        ByteBuffer byteBuffer = image.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[byteBuffer.remaining()];
        byteBuffer.get(bytes);
        InputImage result = InputImage.fromByteBuffer(
                byteBuffer,
                image.getWidth(),
                image.getHeight(),
                rotation,
                InputImage.IMAGE_FORMAT_NV21
        );
        return result;
    }

    public static InputImage fromImageToInputImage(Image image, int rotation) {
        ByteBuffer byteBuffer = BitmapUtils.yuv420ThreePlanesToNV21(image, image.getWidth(), image.getHeight());
        return InputImage.fromByteBuffer(
                byteBuffer,
                image.getWidth(),
                image.getHeight(),
                rotation,
                InputImage.IMAGE_FORMAT_NV21
        );
    }

    public static InputImage fromBitmap(Bitmap bitmap) {
        ByteBuffer byteBuffer = BitmapUtils.convertBitmapToNv21Buffer(bitmap);
        return InputImage.fromByteBuffer(
                byteBuffer,
                bitmap.getWidth(),
                bitmap.getHeight(),
                0,
                InputImage.IMAGE_FORMAT_NV21
        );
    }

    public static InputImage fromBitmap(Bitmap bitmap, int rotation) {
        ByteBuffer byteBuffer = BitmapUtils.convertBitmapToNv21Buffer(bitmap);
        return InputImage.fromByteBuffer(
                byteBuffer,
                bitmap.getWidth(),
                bitmap.getHeight(),
                rotation,
                InputImage.IMAGE_FORMAT_NV21
        );
    }
}